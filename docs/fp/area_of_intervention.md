Area of intervention:

#Mycoremediation

There are many projects producing sustainable processes with recycling systems generating 0% damage to the earth. But I realize there is also a problem of the materials that we are producing daily that can´t be recycled, the unrecyclable materials.

![](assets/A_2.jpg)
*Mycelium growing starting to digest a cigarette filter.*

I looked forward to solve the problem of unrecyclable materials that we are producing daily. They are filling the dumps, damaging the planet, creating toxics for the earth that don’t have an end date and can´t be reused.  My idea was to look for a better way that these materials going to landfill could support not damage natural processes.

##Fungi world

![](assets/A_1.jpg)
*Mushroom in Valldaura.*

Going toward this idea I discovered the fungi world, learning that they are the digester of matter in nature in charge of breaking down species after death and reintroduce them to the soil.

Mycelium, are like the roots of the mushrooms and in the fungi kingdom the tree is the mycelium and the fruit is the mushroom. Through the mycelium the fungus absorbs nutrients. Secreting enzymes of the food source breaking down biological polymers into smaller units and then absorbing them.

I am currently guided by the mycologist Paul Stamets. He believes that "mushrooms can save our lives restoring our ecosystems" and I learnt about the digestion system of mycelium called mycoremediation, that is able to break down artificial materials.

![](assets/A_3.jpg)
*My display of the process of mycoremediation.*

I went through an experimental process of training fungi to digest unrecyclable material. The aim being to to rejoin these now useless artificial polymers to nature.

I took as starting point street cigarette butts, a very toxic and unrecyclable material that we can see in cities and in the ocean.

![](assets/A_4.jpg)
*My outcome of my first mycoremediation process, making a oyster mycelium digest a street cigarette butt.*

I developed a training system where I first fed mycelium with a new cigarette filter so it creates the necessary enzymes to break down the new filter. Then after it has digested them completely I introduced a used cigarette butt from the street of Barcelona and because it had just created the enzymes the mycelium was able to breakdown the used filter and digest it completely.
Creating ecological system, where I hack nature to join artificial materials making a circular disposal process.
