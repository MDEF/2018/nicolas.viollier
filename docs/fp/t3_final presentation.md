#Final Project

![](assets/FP_18.jpg)

*Regenerative Design. The biology process of repurposing matter.*

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/loH0oSCw_eE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

*Final project video.*

##Context

The amount of waste that the world is producing is outrageous. The average citizen generates 1,3kl of trash a day and the world generates 10.000 millions of tons of waste in the year (Gringer, 2018). 79% ends up in dumps, 12% incinerated and 9% recycled ("El 91 por ciento del plástico que fabricamos no se recicla", 2019).

All this amount of waste comes from artificial materials like plastics, metals and glass. Where most of them have a very short time of use with long degradation time. By 2050, the world will be full of toxins where more than 70% of nature has been interfered with changing the whole ecosystem (Burke & Townsend, 2002).

These waste materials are compounds of toxins like phosphates, dioxins from PVC, phenols, fluorides, hydrocarbons from petroleum materials like plastics (McCoy, 2016), infecting our planet, infecting and modifying the earth, creating big changes in the natural cycle and the food chain.

Of the 118 elements in the periodic table 53 are considered heavy metals.  A common presence in industrial waste having a detrimental impact on soil, plant, and animal health. Some of these are beneficial to living organisms in small concentrations but act as toxins when their concentration is above a certain amount (McCoy, 2016).

Heavy metals are found in rocks, soils systems, and bedrocks. Being naturally released from volcanos and erosion, into the environment in low concentrations having minimal impact. But in areas affected by industries, mines extraction or waste deposition, toxic concentration of these elements arise creating negative consequences in the surrounding environment (McCoy, 2016).

Agrochemical fertilizers, sewage sludge, pesticides, metal mining, smelting and oil and gas operations are some of the main sources of heavy metal pollution (McCoy, 2016).

Our planet is running out of room and resources.  Humans have destroyed more than a third of the natural world. A study of the World Wildlife Fund (WWF) warns that the human consumption levels are plundering the planet at a pace that outstrips its capacity to support life. By the year 2050 an extra planet will be required because the existing resources are exhausted (Burke & Townsend, 2002).

Nowadays sustainability is the solution to the exploitation and contamination of earth.  Looking for a new type of consumption meeting the needs of the present without compromising the future generations. Developing solutions that look for a circular process where the materials are always in constant functionality. (Girardet, 2013)

But after 25 years of this sustainable age, the problem hasn’t been fixed. With an economic system that occurs under the rules of capitalism we have created many fake solutions that don’t really solve the problem, like the popular recycling system of materials and objects (Girardet, 2013). Where most of the times we produce more contamination in the recycling process, transportation and milling and casting, than the damage that it might generate in a dump. Or the idea of biodegradable plastics to solve the ocean contamination, was defined by the UN as false. It is extremely durable being only able to break down in temperatures over the 50˚C and are not buoyant, so they are not exposed to UV light. The result is creating ocean currents of plastic spread from the Arctic to the Antarctic.

"We need to start thinking of regenerative rather than just sustainable development. We urgently need to take specific measures to help regenerate soils, forests and watercourses rather than just allowing them to be sustained in an increasingly degraded condition as we have done for many years (Girardet, 2013)."

![](assets/FP_1.jpg)
*Water contamination.*

##Area of intervention:

The Fungi Kingdom is one of the biggest and most important decomposers in nature, responsible for transforming natural materials into simple organic forms that are then food source for many other species. An agent, that is able to return essential components into the loop for the reconstruction of living beings. They are essential for the natural cycle, because they are able to decompose various complex natural polymers (Watson,n.d.).

Paul Stamets (2000), a mycologist, in his book "Growing Gourmet and Medical Mushrooms" defines Fungi as the premier recyclers on the planet. The enzymes and acids they secrete degrade large molecular complexes in simple compounds. The end result of their activity is the return of carbon, hydrogen, nitrogen and minerals back into the ecosystem in forms usable to plants, insects and other organisms.

Fungi are able to digest almost everything, having the ability to store, release and accumulate toxic elements. They are proficient bioremediators capable of breaking down long-chain toxins and metals able to neutralize or remove contamination (Stamets, 2000).

Mycoremediation, myco (fungus) and remediation (clean, resolve or correct), is a biotechnological application that uses fungi to remove environmental and industrial pollutants. Mycelium, the mushroom root, recognizes the contaminations and then secretes extracellular sensitive enzymes able to break down the chemicals into non-toxic compounds remediating the artificial materials contaminations cleaning and neutralizing the environment (Rhodes, 2014).  

Mycelium can be used as a new type of solution to waste pollution creating a regenerative system. They allow the biodegradation of waste integrating it back to nature. This presents a better and future solution for contamination instead of fake sustainable ideas.

For this method to work there are many growth factors that need to be present in the environment: E.g. temperature, humidity, nutrients, and oxygen (Dutta & Hyder, 2017), it is also necessary to develop a process of enzyme generation, by which the mycelium breaks down the toxins of the contamination.

Mycelium are able to learn about the toxins present in the environment, generating the correct enzyme to break down the contamination agents, digesting the toxins and transforming them into organic nutrients ("Mushrooms Can Digest Cigarette Filters", 2014).

One of the big problems of this process and why it hasn’t been adopted nowadays is that the process of remediation of fungi is too slow to be embraced on an industrial scale. In cases like oil spills in the ocean or wildfires that leave big amounts of toxins in nature, mycologists have proposed mycoremediation solutions, but the federal regulations of the city always require removal of 100% of the contaminants in a short time(Alexander, 2019).

Peter McCoy self-trained mycologist and founder of one the most popular online mushroom community named Radical Mycology Mycelial Network and writer of the book Radical Mycology: A Treatise on Seeing and Working With Fungi, teaches a way of training mycelium to digest cigarette butts in a faster way (Mycology, 2014).

The process involves feeding the mycelium first with a new filter so it learns about the new material.  The mycelium recognises that is made from cellulose acetate that comes from the chemical decomposition of tree cellulose and because it does not have toxins, it creates the correct enzymes, and starts breaking it down. After the fungi has decomposed the new filter completely he inserts a used cigarette butt that has toxins. The mushrooms have memory and have developed the enzymes before, so they were able to break it down quickly in some weeks, removing the toxins and transforming contamination into organic material (Mycology, 2014).

This process developed by Peter McCoy could be used for other cases to remediate different contaminants in nature like industrial and food chemicals, boat fuel, agricultural and forest pesticides, ocean plastics, etc. Having as a base what he names the "Mycelium training process”.  The feeding of the fungi with some neutral material that does not have toxins, so it will be able to learn and develop the enzyme to then insert this mycelium in the contaminated environment of the same material. If you insert the mycelium immediately in a contaminated environment the process of breaking down will be slow making difficult to develop the correct enzyme.

![](assets/FP_2.jpg)
*Mycelium.*

##Description of intervention:

My aim is to create an intervention for the future solution of waste contamination, developing a bioremediation system to regenerate the matter and clean the earth.

Why not create a type of fungi capsule that is pre-trained and grown for then when they are healthy to leave them out in a contaminated space to start remediating the toxins?

I will develop a mycoremediation training system that promotes the neutralization and remediation of pollution in nature. It aims to provide different entities like the manufacturing industry, agriculture, restaurants, crafts, or other agents who have a constant production of toxic waste, that ends up infecting nature.

Another component of the project is the design of an fungi incubator that proposes a real solution to the anthropogenic epoch, opening the possibility to create a remediation system. The object will have a humidity and temperature regulator and sterilization system that will help the process of growing mycelium wherever you want. The mycelium will be inserted in a type of myco-capsule that will be fresh and grown healthily before being ready to remediate.

![](assets/FP_3.jpg)
*My experiment of a Oyster Mycelium breaking down a street cigarette butt.*


##The Future is Myco-Capsule

With the intention to take out of the lab the mycoremediation experiment I look forward to a functional way for it to be used in the real world. Realizing that there are many laboratory test and research projects about the mycelium remediation, but scientists always developed in a "petri dish scale" too tiny and difficult to bring to functional solutions, I realize that my innovation needs to be in design a product/process that helps use this process in the real world.

This will be the "Myco-Capsule", pills able to eliminate specific contaminants containing in pre-trained mycelium for that specific toxin.

My project will consist of a futuristic company service that grows myco-capsules, previously grown in a designed training incubator, in a sterilized space with the correct humidity and temperature to make it grow healthy. It starts from a fungi spore, developing the correct enzymes to break down the toxin that it will bio-remediate afterward. It grows until the mushroom is big and strong to get out of the incubator and ready to be inserted in a contaminated place to neutralize the toxins.  

This capsule will be base of a mixture of agar agar, a gelatinous substance from seaweed that is like the media mushroom cultures, malt extract, that is the rich liquid nutrient food source, some type of grain, sawdust or lignocellulose, that give to it strong nutrients to support the growth of the fungal ("Growing Mushrooms In Coffee Grounds", 2019) and some neutral material, without contamination, (like the new filter in the case of the cigarette but) to pass through the mycelium in to the training process to break down then the same material but contaminated.

![](assets/FP_4.jpg)
*A render of the design of the capsule.*

##My Intervention in the Future

It is 2050, named the age of the Bio-Tech Science, where projects that mix technologies with nature are the base of every innovation. Where they join robotic production with biology systems and intelligence to solve human necessities, like buildings made by ants, amazon bird messengers, mycelium packaging, growing orange peal boxes, mushroom printers, etc.

The regenerative design is the main topic and sustainability is something vintage or old school. The capacity to crop or grow in a controlled way helps to design projects that have 0% pollution and are part of the natural circular material cycle of biology where everything is born and then dies to be a nutrient for another species.

Mycoremediation process is the saviour of the time, thanks to it the world is clean of toxins where more than 80% of human contamination has been neutralized.

It is the biggest business, in the last 30 years scientists joined with designers and have developed this area that Paul Statement started in 2010 in a huge way.
Developing specific species of fungi able to break down each material and neutralize their toxins, they are perfectly trained to recognize immediately the waste food for what it has been made, regenerating material to nature to be part of the ecosystem.  

The contamination problem, that previously has infected the whole nature, is something solved and is not a theme now. There is still small quantities of pollution in the earth, but the citizens don’t see it as a problem because they know that mycoremediation is something effective and in some more years it will be neutralized and the planet will have a 0% toxin production.

This bioremediation processes with fungi started just before 2030. It began in public with a DIY kit sold on Amazon, where you could mycoremediate used cigarette butts from the street. Growing mushrooms from them. A tester came in the kit, with the ability to check the remediation of toxins in the filters and determine whether the chemical of the cigarette had gone. The kits showed the possibility of the regeneration of a material, where you could neutralize contamination and provide rich nutrients for another species.  

Now the cities are designed with myco-canals, where they have been designed urbanistically with mycelium connection underground. There are special parks where there are many types of fungi where you can leave your garbage to be neutralized. The street recycling bins have been replaced with myco-bins wherein each of them grows special mycelium to remediate some common materials like glass, cans, paper and plastic bottles. You can see everywhere mushroom growing to digest your contaminations that are connected to different spots to transfer the enzymes to remediated everything.

The myco- capsules are the innovation of the time, shops sell different types of it like you buy a condom, a snickers or a lighter near the A.I. clerk. You can buy these capsules of fungi remediator that are trained and grown before to be able to digest some specific material contaminants. So then in your house, you can throw, after reading there instruction, this myco-capsule and it will start to grow to break down chemicals. If you are lazy you can buy your myco-bags that have in their textile mycelium spores that will break down the contamination more easily .  

There would also be design companies of myco-capsule for specific environments, creating ones with particular materials, shapes, and fungi spores to remediate specific contaminants present in certain spaces, creating a service of custom myco-capsule. Like the ones designed to float in the ocean to clean the oil split or ones resistant to low temperatures to remediate the contamination in Arctic citys.

Custom myco- capsules are commonly used in natural disasters like earthquakes, wildfires, and tsunamis and for industries that produce or use chemicals dangerous for nature.

The EU has recently created a law where every manufacturing industry needs to have it's own a myco-landfill to remediate all their chemicals produced, then all their contamination is transformed into natural material. It is now common to see a mixture of a landfill and culture farms near manufactures, with mushroom growing from them.

The sustainable certification has been replaced by myco-certification, you can see on the shelf of the shop's product whether they have this certification that means a product can be digested by some fungi, and ones that do not have it. Also with your phone or your scanner glasses, you can check where or how you can throw this product and its packaging, be it in the streets myco-bin, in the myco-parks or buy a special capsule.

The problem now is there are many types of mushrooms everywhere in the city and some times out of control, discomposing old building, materials that you want to keep and at worst sometimes infecting peoples bodies. The government is looking for a system of controlling the spread of the mycelium. In the past there were garbage collectors, now they are creating organic disinfectors, which are automatic cars that tray to prevent the mycelium spread.

The fungi kingdom also consumes oxygen like humans and generates dioxin of carbon, so it is necessary to have trees near the cultures of mycoremediation. Scientific Tech had developed specific types of trees that are able to consume the mushrooms pollution in a fast and effective way that is starting to grow near the myco-parks decontaminating the space.

![](assets/FP_5.jpg)
*Myco-capsule brand, the “Remediator”, device that is sold everywhere to decompose different materials easily in the house bins. You can find it in the detergent shelf of the supermarket.*

![](assets/FP_13.jpg)
*“Remediator”, is the commercial name for the brand of the myco-capsule company. The branding wasn’t searching to be a Scandinavian design it was looking to be a popular commercial design with an identity from the actual cleaning products to make it looks like part of the detergent shelf of the supermarket. To create a product closer to the reality imagining that is a normal product of nowadays.*


##Reflections

Nature during it 4.600 million of year on the earth (Gómez,n.d.) has developed a whole process of creation, manufacture, treatment, and decomposition that we can name as ecosystems. This ecosystem is perfect, everything is an essential piece of the puzzle where every species has a task to make this puzzle work correctly.

Waste in nature does not exist and either contaminant, everything is a piece of nutrient to other one and any matter is valuable, there aren’t biomasses more appreciated to other ones there are different species that appreciate different ones.

The mutualism is an interaction between organisms of two different species in which each of them is benefits from the interaction in some way. Many times involved and exchange of resources, like shelter, food, nutrient’s or the exchange of services like protection, transportation or healthcare. Being an important role of the ecology ("Mutualism - Definition and Examples", 2017).

The moray eels are a big carnivore fish with big and sharp teeth’s that lives in dark sea caves. The problem that it can’t clean his teeth’s so in exchange for protection the red shrimps take this task who also live in the rock shelter ("10 ejemplos increíbles de mutualismo animal", 2016).

![](assets/FP_14.jpg)
*Mutualism example.*


**Why do humans create their own ecosystem that harms nature?**

We just need to be part of that ecosystem but the Anthropocene era has developed a system where humans are the center of the planet disrupting   all natural systems,. Making a process which in parallel of the natural ecosystem that ensures only human necessities are met, and not joined with biology’s needs and rules, damaging nature disturbing its network.  

How can humans be part of this ecosystem?

I see the beginning era of biodesign as a  future weak signal to join human needs to  nature’s network, like an open window to incorporate living organism as essential components in design and fabrication.

"Biodesign leaps ahead of imitation and mimicry to integration and use, dissolving boundaries and synthesizing new hybrid objects and architecture. Replacing industrial or mechanical systems with a biological process, an approach becoming more important under the pressure of the climate crisis." (Myers & Antonelli, 2014)

My project aims to be completely within the area of biodesign, with the organisms as an actor of change positioning as a solution to  earth’s contamination, but there are a million more opportunities for bio-projects. When you get in natural systems is incredible the amount of surprise that it gives us tools with a powerful intelligence that exceed any human creation.

The most essential point is a human change of mentality where we trust, believe and admire the ecosystem having it in the first line to work with, respecting its times, workflow and energy.  Where the biological process is considered as a viable alternative to conventional technologies. Thinking more like mutualism projects, we give to them something and they reattribute with an output.
