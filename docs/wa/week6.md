#State of Art of AI

##What is intelligence?

The definition of intelligence for Oxford dictionaries is " 1. The ability to acquire and apply knowledge and skills. 2. The collection of information of military or political value.

For us, intelligence, after a group analysis work, is the ability to link and create logical thoughts, that comes from a before process of learning that then is developed in the brain for then being used in different ways.

##Artificial Intelligence

Artificial intelligence (AI) is the area of computer science that create intelligent machines to perform tasks commonly associated with intelligent beings, applied for projects developing systems endowed with intellectual process characters of humans, like reason, discover, research, answer, learn from past experience. (R. Encyclopedia Britannica)

![](assets/A8.JPG)
*Barcelona air quality control system.*

The AI is made to solve different type of life problems transforming jobs that are a waste of time and not efficiently made by people who are worn down, like phone assistant, a data selector, trash collectors, labor’s, etc. These workers can be changed by robots that can make the same work more efficiently without personal issues, only turn on this moving computers and they began to work. Use the time and skill of the people to make other works more dedicated to the creation, organization, and administration, making the stressful work madden by machines.

Nowadays the are many companies creating all type of AI invention with a different type of functionalities searching for the same objective use computers to make better a human work.

One good example is the Japanese insurance firm Fukoku Mutual Life in 2017 replaces 34 employees for Artificial Intelligence machine of IBM named Watson Explorer that will calculate payouts to policyholders believing that will increase 30% of the productivity. This IBM AI computer can think like a human analyzing and interpreting all the data of the customers including unstructured texts, images, audio, and video.

![](assets/A7.png)
*IBM AI Watson Explorer platform.*

In the other side, the discussion is that the big issue that would generate in the future AI making our life better is the fewer job opportunities that people would have. But PricewaterhouseCoopers (PwC) argued that it is not true, Artificial Intelligence would create more than 7 millions of new UK jobs in the areas of healthcare, science, and education in 2037.

![](assets/A9.jpg)
*Pepper, world’s first social humanoid robot by Softbank Robotics, who inform and guide visitors in an innovative way in Belgium Hospital.*

PWC argue that AI technologies would replace human workers but also create many additional jobs because productivity would increase and the developed products would rise so companies would need more people. Education and healthcare would be improved so society increases it demand and also all ways needing always in this type of service a human touch. (R. The Guardian)

##Google the AI leader

But the leader in this them it is Google was the CEO Sundar Pichai says, "AI is one of the most important things humanity is working on. It is more profound than, I dunno, electricity or fire". This company has the most amount of information about people, research’s, web information, pictures, videos, etc. So they are using all this information first making an intelligent search service for their users but that is the first start they are also creating all type of intelligent machines with different objectives.


###Some of the new Google AI developments:

**Google AI** is a platform were they showing and teaching a different type of simple AI Google tools for people that are not experts opening these new technologies for everyone.
![](assets/A10.png)
*Google AI platform.*

**Google DeepMind** a program that thinks like a human using previous knowledge to solve fresh problems. The machine does not forget how it solved pas problems.
![](assets/A1.jpg)
*DeepMind playing chess.*

**Google Duplex** is a virtual voice assistant that can make phone calls for you, scheduling appointments, booking hotel, reservation in restaurants, etc. This AI system is making very real human voice adding "er" and "mmm-hmm" where you don’t realize that is a robot.
![](assets/A2.jpg)
*Google voice assistant.*

**Google Tensor Flow** US Department of Defense are using this AI system to analyze the many of pictures taken by the military drones, detecting objects of interest for then being those ones analyzed for the US DoD human team.
![](assets/A5.jpg)

*Google AI helps US DoD to analyze drones footage shot.*

##Daily A.I. that we use

Nowadays are using many Artificial Intelligence tools that we haven't realize that it has an AI brain in. These are some of them.

**Siri** it is a voice assistant of the Apple that connects with your personal information in your phone, computer or tablet, and the Internet information. It can search some information, write or read a text, make calls, set alarms, reminders, can anticipate what you might need to help you.
![](assets/A4.jpg)
*Sire being us by an Iphone.*

**Social media** like Facebook, Twitter, Instagram, takes your past behavior, web searches and interactions making the feeds that you see in your timeline curated by an AI machine to then show you the most interest information for you.  

**Spotify** it is very similar that social media, you feed them with your different interest so they then create the material with AI for you like Discover Weekly, or playlist recommendations, or introduction to some new artist. So you never get bored with the app.
![](assets/A12.JPG)
*Spotify Daily Mixes.*

**Bank Fraud Protection** this type of finance service learn about your normal movements with an AI computer. So if the notice that you have a not normal activity like take a big amount of money or buy something out of your country they will let you imminently in case that is a fraud.

**Security cameras** it not possible for humans to keep monitoring multiple monitors with hundreds of cameras so AI makes an essential job here recognizing objects and faces making more efficient the security jobs.
![](assets/A11.png)
*Chinese security camera recognizing the objects.*


##Week project artificial intelligence object

**Name** Sustainable Assistant.

**Goals** report the sustainability details of products making the costumers conscious of the process and materials of what he is baying and make them buy the ones that have best qualifications.

**Context** when you are going to buy something food or an object could be online or in the shop.

**Actors** manufacture, quality controls, users.

**Input** an AI machine scans the products and make the details joining information of the company, Internet information and have also an ultraviolet recognizing the real components. Also, it has a platform where the user comments their experience with the product bought.

**Output** qualification of the suitability details divided into 3 items, components, industrial process, and user experience were you can click on them and have more information about every item.

![](assets/A3.jpg)

![](assets/A13.jpg)
*AI idea, scanning and seeing the results.*

##My vision in the future

**Goal**
Use technologies to make a democratic and sustainable world.

**Objective**
Making designs that encourage self-organization and unite, without altering our interactive and social way of life.

**Direction**
The technologies are all made, we need to search and use them in a positive way were they can help joining and organizing us, looking forward towards a more sustainable and natural world.

**Business**
Create a company that looks to the future in a positive way using new technologies as a tool to improve lives in a democratic manner, but not changing our social way of life.
