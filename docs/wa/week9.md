#Describe your Idea

This week was the moment when we learned to summaries our idea and begin to think in a specific topic that we will go through. With the different process, we will learn how to describe our project, the different steps and the final result in an engaging way.   

In this point, we need to begin to experiment and stop making a global research Identifying your niche, see the state of art of your them and become an expert in your field of interest.

So my idea of interest at that time was:

**Create a circular material with an unrecyclable object**, like a cigarette butt, joining it with some organic materials.


##State of art of my topic

Going through my idea of interest I began to research on that topic, looking for examples and reference were they are doing materials or objects with new materials that join nature.

![](assets/4N.jpg)
*Snicker made form Kampuchea by Jean Keane.*

This is Grown, its a change or have we actually are managing the industrials textile. Jean Keane has grown k, rhaeticus bacteria (Kampuchea) creating a new type of microbial weaving, making trough biotech a Sniker made for a microorganism.  

Mixing different types of bacteria’s and yeast to optimize their natural properties and create hybrid materials that are strong, light and potential to a Nano-scale. Looking for an alternate could help us not just replace petrochemicals-bases materials but also change our manner of manufacturing.

![](assets/5N.png)

*Create a bacteria that eats cellulose and produces electricity.*

The U.S. Department of Agriculture discovered that if you match some types of bacteria’s that are in the stomach of termites and ruminants, they can create a fuel cell that consumes cellulose and produces electricity. Cellulose is the largest amount of biomass produced by plants so we can transform it into energy.




###BioPlastics

IAAC and Fab Lab center of the textile are designing new types of fabric made from natural materials, looking for alternative textiles that are part of the sustainable system of nature. Creating a new types of plastics 100% biodegradable that does not disturb the natural cycle.

Human have created more than 300 polymers and nature in all his history have only needed 5 polymers, so why not use the thing that we have in-house, like a leaf over vegetables and fruits, to create their own polymers that then you thrown them to nature go ins the normal natural cycle.

This is some of the biomaterials that they have created:

![](assets/6N.jpg)
*Biopolymer made of matte tea.*
![](assets/7N.jpg)
*Biopolymer made of mango.*
![](assets/8N.jpg)
*Biopolymer made of Kampuchea.*
![](assets/10N.jpg)
*Biopolymer made of orange.*

##Going Trough Fungi World

After learning about different manners that we can intervene and mix different component’s of nature in a sustainable way creating a perfect solution of a circular natural cycle that would generate a clean world, instead of exploding it like nowadays, where humans life to generate 0% damage.  

I went in more focus my research on how I can solve the problem that I have discovered how to join to nature with these unrecyclable materials that we are generating. Focusing on how to solve, as a first step, the problem of cigarette butts. So after talking with classmates, teachers, analyzing mater fab visit and the last lesson I went through the world of the Mushrooms.  

![](assets/11N.jpg)

*A mycelium, they are like roots of the mushrooms but in fungi kingdom the three is the mycelium and the apple is the mushroom.*

Fungi are part of the Fungus Kindom eukaryotic organism composed of mycelium, a single spore that comes from the mushrooms germinates mycelium. Trough the mycelium the fungus absorbs nutrients, do it in two stages, first, the hyphae secrete enzymes of the food source breaking down biological polymers into smaller units absorbing it.

###Bio-Designs with mycelium
In Materfab library of materials of Barcelona, you can see many sustainable actual functionalities using mycelium. This is some of them.

![](assets/1N.jpg)
![](assets/1N_2.jpg)

*Strong packaging made of the cultivation of mycelium in agriculture leftover.*


![](assets/2N.jpg)

Isolation system panels joining agriculture leftover with mycelium.


![](assets/3N.jpg)

Board, similar to an MDF board, made of the cultivation of mycelium in vegetal fibers.

###Mushroom eating synthetic materials

![](assets/12N.jpg)
*Paul Statements, fungi scientist believe that mushroom can slave the world.*

Paul Stamens propose that mushrooms can save the world. He said that mycelium, that are the roots of the mushrooms, can cleaning polluted soil, making insecticides, treating smallpox and even flu viruses.

He proposes that mushroom could solve the world trash explosion. Explaining that fungi’s are intelligent and learn how to extract food converting different materials into sugars.  Making and experiment were we grow mushroom in piles of saturated diesel. Where the mycelium absorbs the oil, produced enzymes breaking the carbon-hydrogen bonds remanufactured into carbon hydrates that are fungal sugar nutrients for them.

![](assets/13N.jpg)
Mushroom growing in oil.


###Way, not mushroom eating cigarette butts?

Cigarette butts are compounds of paper and filter, the filter is made from cellulose acetate fiber.  Cellulose acetate fiber is a type of plastic, one of the newest synthetic fiber, come from cotton or tree pulp cellulose but it does not biodegrade but is a simple composition modification from a plant cellulose.

Then it began to make sense that mycelium could eat cigarettes butts. Researching I discover also that one of the principals and accessible nutrient source that fungi degrade in the natural world is it cellulose for plants and trees.
So fungi could eat cellulose acetate because they use digestive enzymes to break down cellulose into a simple sugar that then is metabolized by fungus.

Discovering a scientist has **learned how to train mushrooms to eat used toxic cigarette butts**. He introduces a new food to the mushrooms but in a novel way a new cigarette butt, so the mycelium could first determine the food and the produce the correct enzymes and began to digest. Then when the fungi are used to eat this new food and have generally the sufficient enzymes he introduces the toxic used cigarette butts and began incredibly to digest it.  
<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/fCAX9P50SNU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

*Experiment process of growing mushrooms with cigarette butts.*

You can use this system of **training mushroom to eat different toxic, industrial chemicals, petroleum and also plastic.** All the materials in one point come for nature fungi kingdom can go in them and learn about the materials and digest it.


##Learning how to go on Kickstarter
Heather Corcoran leads outreach for Kickstarter in the UK with focus on Design and Technology communities. Teach us how to work in a kit starter project and teach us to go improves and show in an engaging manner our project.

###Steps for a Kit Starter Video
Something very important in a Kit Started is to have a very good convincing promotion video.

Video details:

1.    15 seconds, of a, summarize of your project.
2.    15 seconds, of bringing experiences or mission.
3.    30 seconds, who you are and your personal project.
4.    Dive dipper in the project.
5.    Don’t forget good light and sound.

###Summarize your project
To make your project with a strong narrative and captivated you need to start summarizing it in a title, subtitle, description and the main image.

This was the result of my summarize:

**Title:** Smoking Mushrooms

**Subtitle:** Grown your own Fungi with cigarette buts.

**Details:** Cigarette buts are made from cellulose, and cellulose is the most accessible nutrient source that fungi can a degree in the world. The idea is to join them and make mushrooms with the most common litter waste product.

![](assets/14N.jpg)
*Image used to present my idea.*



###Reflection
I haven’t know much about Kit Starter and Heather with her experience help us to go make more real my vision topic. Imaging it in like future real project making us think more in its final result like a product that needs to be sold. Opening my mind about what type of projects success in nowadays trough crow funding and how to communicate your ideas. Realizing that many times you have very clear what you want to do, but haw to tell in a convincing way it is difficult, so taking references of successful Kit Starter is the best guide.  







##Knowing an App Developer
Bjarke Calvin is an App Developer and part of the MIT Doclab, described like a Studio of the Future of Documentation, teach us the value of the image.  Teaching us that we are in the Age of the Photography it is no longer the books and magazine it is the visual documentation.

He is the funder and CEO of the App Duckling. This application is storytelling documentation, based in mini stories of pictures and videos, with the idea that we all have a good idea to tell. Duckling is an app that join us, evolve us, change the selfie for a bigger and more powerful picture.


###Exercise, Make your Duckling Storytelling.

![](assets/15N.jpeg)
####My Duckling storytelling.[Press here to see the whole story](https://duckling.me/nviollier/5784652301290).
*My Storytelling made in Duckling.*


Reflection
It is difficult to create a story that is attractive to others that do not know anything about your project. So the order that you show your information pictures is very important. First show an attractive one for the presentation slide, with a strong image, and then make chronologic order that makes the people go in the story and kip their attention that they what to reach the final result.

Also, adding word is not bad because sometimes little fraises can help to focus all the information that you join in the picture.



##My Storytelling

Then at the final step of the week, we need to create a storytelling of our final project so I decided to create a storytelling that instead of a typical digital video or graphic one y make a physical showing with real material my story.

My story was about my research, explained before, "How to train mushroom to digest cigarette butts" looking to join human materials to the natural cycle.

So, I made this physic storytelling with IKEA Ziploc bags using them like slides or steps of the project. Where I put in a different thing like real mushrooms, cigarette butts, and fake mycelium, that explains the process of the experiment and way it would work.

So here I show in order the different information of the bags in order and on italic what they have inside.

1.    4.5 trillion cigarette butts littered each year. *Many used cigarette butts of the streets*
2.    Cigarette filters made of cellulose acetate, Not Biodegradable (10-15yers), but simple composition. *Pulp of used cigarette butts filters*
3.    Cigarette Filters sterilize a compound of plant cellulose modification, but toxic. *Sterilize used cigarette butts filters*
4.    New filters, without cigarette toxic components. *New cigarette butts filters*
5.    Training mushrooms with new filters to create the correct enzymes. * Market mushrooms, new filters, and fake mycelium*
6.    Introduce use filters, with the enzymes just crated. *Market mushrooms, used filters, and fake mycelium*
7.    Mushrooms eating break down cellulose into simple sugar and then metabolized. *Market mushrooms, sawdust, and fake mycelium*
8.    Mushrooms, highly intelligent structures. * Market mushrooms*


![](assets/16N.jpg)
![](assets/17N.JPG)
![](assets/18N.JPG)
*Some of the bags of my storytelling.*


![](assets/19N.jpg)
*My whole physical storytelling.*


![](assets/20N.JPG)
![](assets/21N.JPG)
![](assets/22N.JPG)
![](assets/23N.JPG)
*The chronological order of the story.*

###Reflection
Was an excellent, easy, visual and cheap manner to show my idea, the research and see with the real materials the process and the result. Having in the two extremes very different things in one-side cigarette butts and in the other side mushrooms, showing in the middle that they have something in common. So you can play and change the orders speculating.

Realizing, after the teacher's correction, that the most important and valuable thing about my project is in the middle of my storytelling realizing that I am training the fungi to eat the synthetic material. **I am hacking the nature** to join our materials to their diegetic cycle.

So the next step will be to prove my idea and also speculate future scenarios with this new connection with nature.
