#Making a Robot

During this week we generate group documentation of the construct and design details and process of the machine.
**[Press here to see the documentation](https://mdef.gitlab.io/animal-farm/).**

##Our Final result

PollockBot is a drawing robot that draws by causing vibrations of the two motors, it is dependent on a light sensor. PollockBot reacts to its environment by detecting the movement of the LED lights that surround the base.

![](assets/B1.JPG)

![](assets/B2.jpg)

![](assets/B3.jpg)

![](assets/B4.jpg)

![](assets/B5.jpg)

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/8AbGLQm4qxI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
*Pollock video for my Instagram account.*


##My Contribution

###Sytem of vibration

I was in charge of the creation of the vibration system that will move the machine. We start researching examples of DIY vibration were they tell us to put a piece of paper in one side with tape in DC motor realizing that wasn’t so complex. So we doing with cardboard and a gray tape having an excellent result, making us tested for the first time the machine moving and drawing.

![](assets/B6_2.jpg)
*The first prototype.*

![](assets/B6.jpg)
*The second prototype that convince us more.*

The to create a vibration motor more solid and control we 3D print 3 different models, two rectangles one more large than the other and the third one more circular organic, having a hole in its extremes of the diameter of the stick of the DCmotor. Having the best result with the circular organic design, to generate a stronger vibration and it fits very tight to the motors.

![](assets/B7.jpg)

*The tree 3D model that I print to test the vibration system.*

###Shape design

When we have defined how it will work, the functionalities and the main pieces, was the moment to define the shape of the robot. I realize that an octagon geometric will be good having space to levee the electronic parts in and using the different sides to leave the motos and the markers. Having sides larger and other more charters.

![](assets/B8.JPG)
*First sketching of the shape.*

####Electronic movement system
I was in chard also with another member of the team of creating the electronics of the robot, the objective was to create an interaction between the moving light of the base and the DC motors. Where the idea was when the motors have a light in front of them the motors move. So after many test and electronic connection, we make it connecting light sensors near the motors independently to the Arduino and the Arduino connected to a relay that switches and put on the energy to the motors when they receive light.  

![](assets/B9.JPG)
*Electronic system with two light sensors that activate the DC motors.*

#### How to fit the technology in
Las step, before we cut the definitive shape in wood laser cut, was to measure all the electronics parts of the machine and make the design of the robot is where all the pieces fit in correct position. So we make the electronic design with the lees volume possible little cables, little protoboard, training to have as the most things sold and then I measure all the part, the DC motors, the relay, the, and Arduino. The idea was fit all the electronics in the middle and the motors in the top base.

![](assets/B10.JPG)
*The dimensions of all the electronics parts.*

Having then many problems when we try to fit all the electronics in the final shape, because we don’t realize that the cables use more space and if they are so tight any movement will disconnect the system. So we move the relay to the top base without being able to cover the back wall.

![](assets/B11.JPG)
*Traing to fit all the part in the box was very difficult.*


##Important reflection

Looking forward to my personal project these ones of the most important steps that I have learned:

###Playing with materials give you ideas
Our group idea ways to make a machine that draws but we have many ideas and all of them we like it but we couldn’t connect together. So we have a big mix up of ideas where we couldn’t go through something concrete.

So we began to play with materials, like in the week living with ideas, joining different objects and makes them draw. Incredibly concrete ideas come to our minds and they began to join us in one line of creation that then we went through it.

###Testing is the only way to see if it works
You can think, research and draw all your ideas but if you do not begin to create it physically you will not be able to figure out if they really work.

###Take ideas from the internet as a first step but then make it your type

We have at first the problem that we were looking to so many references for drawing robots creations and they began in one time to mix our heads. So we decided in one moment to stop researching ideas and began to our own creation with our creativity.

###The shape is something that needs to change

After we have defined our idea we began at first looking for some special shape for our robot, but then we realize that that was very bad because it will change all the time after testing. Because always you realize that you need to put or take something and that will change the shape. So when you have defined all its movements, pieces and electronics then you can define it a more proper shape.

###Electronics is something that needs to be precise and need space

We realize that electronic doesn’t give you opportunities to be wrong, they need to be very precise and very good made because or else it will not work. Also, they need a correct space and no make them fit in a very tiny space.


##For my personal project

Before a week of learning, playing and mixing electronics and materials to make some type of interactive machine, I wanted to think in the future use of it for my personal project.

We I am in the process of growing mushrooms we need a special and constant ambient temperature to make them grow healthy, powerful and faster, for growing Oyster Mushrooms you need 25C˚. Also, a system that sends me data of the state of the fungi, his grown, his food, the temperature, the humidity, and all other useful information for creating a more control experiment. So then I can replicate exactly without problems

An incubator machine, that helps me control all the environmental factors that I need to make it grown healthy and control. Where I can have shelves where I can leave mushrooms that I do not want to grow more and other spaces where I want to make them grown more quickly. This robot sends me daily information of the status of each sample and graphics of the ambient status and other important data that then I will discover that are useful.

There are some open sources DIY projects that can be my starting point.

![](assets/B12.jpg)
*Arduino DIY system to create an incubator [Press here to see the instructions](https://www.instructables.com/id/Environmental-Mushroom-Control-Arduino-Powered/).*

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/2E_kpS37moI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
*Other Arduino DIY example of an incubator for mushrooms*
