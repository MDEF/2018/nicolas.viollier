#Going Through Ideas

Ideas are sometimes difficult to explain and more difficult to get towards. You can spend many days thinking about it and you will never get one good idea that you like, and other days you can have quickly have a good idea and you go for it.

Steven Johnson in his book "Where good ideas come from" explain that an idea is not a single thing is more like swarm of ideas, define a good idea like: "A network of a specific constellation of neurons — thousands of them — fire in sync with each other for the first time in your brain, and an idea pops into your consciousness. A new idea is a network of cells exploring the adjacent possible of connections that they can make in your mind".   

So this week **we learnt how to go into ideas, we began to speculate with them, correct them and to make them real.**

##Material Speculation

The first workshop that we have was about speculating materials in the future, taking one sample of the different materials that we brought to the class, modifying it (break it, join it with other things, open it, etc) and then speculate on what it could be in the future.

I took a floss box and began to open it examine the floss´s functionality. I observed the box, identifying it´s different characteristics and components.

![](assets/I1.jpeg)
*Sketches observation the floss box*

###Components:
*    A sharp knife
*    Floss Box - Assembly of 3 parts
*    Stick
###Characteristics:
*    Ergonomic
*    Hermetic
*    Resistant

I realized that it could be a very good container of a type of valuable filament of the future. That you would need to carry everywhere in your pocket and cut on demand.

So I began to join this floss with some materials similar to a filament like ropes. Then I joined it with a red elastic band, an idea came to my mind it that it seemed to be something powerfully futuristic.

Finally, I joined the concept of this empty floss box and red elastics bands, with a speculation. A hypothetic situation where the food is over in the world and people would begin to eat each other. This would be a box container of blood filament. Where in that time there would be an industry of blood, where people would sell their blood and the blood of dying people would be used. It would be used to create this filament blood that people will carry in this floss box. They would be able to take a little bit and eat it or put it in a blood 3D printer machine and cook something.

![](assets/I2.jpg)
*Filament blood box*

![](assets/I3.jpg)
*Observing the use and interaction of the blood box, trying to eat it in a bar.*

##Make material digital

The connection between digital and physical materials is something that nowadays is very in vogue, but designers are trying to think in a way that material can have digital components inside and interact by itself like a computer machine.

That is not the only way to make digital materials, in class we learnt of a new technology that creates it, but in a manner that the components of the materials are identified by a digital camera. You can interact in a digital way though a display.

In the next pictures, you will see the result of the digital interaction material, the camera captures the color and the texture of the clothes. It changes it for a picture projected onto the selected material.

![](assets/I4.png)

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/rz5GzTFKDH0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

*I put on a green suit and the digital graphic cover all my body.*



##Mater Fad Visit

In this week there was a moment when we are making our projects more real, we began to think about what do we wanted to do, in what area and also what materials. To know more about the materials, their different types, functionalities, creations and open our mind about the innovations we went to Mater Fad.

![](assets/I5.JPG)
*Mater Fad, is a big library.*

Mater Fad is a center of materials, it is a research center and a database for technologies in the area of new materials having a library of more than 5000 materials. It has samples of all them but the most important thing is that it contains information for their composition, characteristics and where to find them.

![](assets/I6.JPG)
*Material mixing aluminum and plastic.*
![](assets/i7.JPG)
*Material made of coffee left over.*
![](assets/I9.jpg)
*Mycelium wood.*
![](assets/I10.jpg)
*Material made of cigarette butts.*
![](assets/I11.jpg)
*Rope made of human hair.*


It was an excellent experience and a moment that opened our minds to see what you can do with different materials, how you mix them and create innovations. **We saw how we really don’t realize the value of trash and how we can learn to use them in a functional way.**


##Futuristic machine

To began to open our mind and put our hands on materials we did an exercise where were given a human attribute, that in my case was "physical activity, the need to move the body". I then created a magic machine magic machine that represented that topic with only some simple materials that they gave us, like plastic plates and glasses, gloves, rubber band, forks, paper, and cardboard.

![](assets/I12.JPG)
*Magic machine that help people move.*
![](assets/I13.jpg)
*Me using the magic machine.*

I created a machine that helps people with limited mobility to move and do exercise. It works by sticking your hands or feet in plates and they will begin to move in a direction, your body will then be able to move in those same directions. The machine will recognize the time that you have been moving, if you don’t move you will not be able to take off these plates and will never stop moving, after a good healthy time it will stop and could take off the plates.

###Conclusion

The exercises make me realize that working in the first start with materials help me imagine new things, make innovation and think in another manner. Taking out of the box functionalities, realizing that the manner that we design can be made in different ways and the future and technologies that are now able to be used and creating magic machines.  


##Start to imagine your project

Now knowing how to create and imagine with materials, the next step was to create a magic machine that represents your final project in a manner of materializing your idea.

So at that time my goal idea was "Use technologies to make a democratic and sustainable world". I this time took an attribute that described my project so I took a paper that said "Idealism, the need for social justice".

![](assets/I14.JPG)
*A magic little toy that hears and write neighborhood comments.*
![](assets/I15.jpg)
*Me and the democratic machine.*

I created an automatic remote control car that goes around the neighborhood and hears what people said like comments of the place and neighbors, recommendations, events, etc. Then go around and paint the walls, special paint that erase after two days, opening the information for all. Making a place where all the people know all the internal news making a join and democratic neighborhood.


###Conclusion

The exercise what a really good way to visualize what you were thinking, and check if it feels correct and innovative. Then go forward with it in a final project.

This step was very important because it makes me realize that the idea of working to democratize society wasn’t something that I would like to do. Also the neighborhood could solve the problem by using a simple WhatsApp group.


##Put hands in your project

After seeing the results of the magic machine of my idea, we needed to begin to make our first real research in our topic, observing and involving myself in the topic that I want to work in.

After the last exercise, I got lost in where to go because I realized that my vision wasn’t where I wanted to go, so I began to observe walking around the street looking in issues that technologies could help and join the society.

Before observing and going walking around many hours I realize that there is a big problem in material recycling in where people nowadays are recycling bottles, cans, and cardboard but they are many other common materials that are not being recycled.

When I observed people recycling I saw cigarette butts being thrown in the streets, so I said to myself what about collecting all the cigarettes butts near my house? They are many of them and nothing is made with them. And many questions began to come to me, I could create a social change? What would people said and look to me? Make a cleaner space? How much time I will spend? What I can do then with it?

###Recollection

The next step was to go to collect them and observe people, so I went around my neighborhood collecting cigarettes butts and talking with people.


![](assets/I16.jpg)
*Collecting cigarette buts, starting first on a side walk and then in a metro entrance leaving the two clean.*
![](assets/I17.jpg)
*The result was a big bag 75grams of cigarette buts in 30 minutes.*

Finally, I clean many streets and a subway entrance, collection 75grams of cigarette butts spending only 30 minutes.

####Learning different important topics:
*    You can easily make a big change in the environment
*    If every citizen, once a year, spends 30 minutes cleaning the street we would have a much more pretty place to live.
*    Talking with people cleaning their own entrance or working in cleaning the streets I realize that they are used to it and haven’t realized that is a society and material problem.
*    Society needs to change the way they use every material is not only the recyclable.
*    Many tiny cigarette butts could make something big and powerful.
*    I don’t know what that very powerful material is, but the collected butts seem to have some good functionalities.  
*    There are many materials that aren’t in the circular cycle of materials; we need to figure to put them in the natural cycle.


###Creation of material

The final step was to make a material with them.


1.    I began to separate the paper and the filter
2.    When I had a big amount of filters I started to open the filters and separate them creating big foam.
3.    I added water and cleaned them
4.    I squeezed it.
5.    I put in wood glue and mix it covering the entire filter pulp with glue and left it to dry.

![](assets/I18-01.jpg)
*Making the material.*


####The result wasn’t good:
*    The wood glue normally dries in one hour but in this case, because I put water after 3 hours it wasn’t dry.
*    To do it I spend 3 hours that was too much for a little square material.
*    What I have made isn´t something really innovative as it is similar to a piece of MDF, and made of a material that doesn’t convince me because it will finally die in the trashcan.

In conclusion, I realize that we have a very big issue with materials that aren’t recyclable, **we need to search for some system that allows them to join a sustainable cycle. Simple and tiny cigarette butts collection**, with a useful functionality, can make a big social change.
