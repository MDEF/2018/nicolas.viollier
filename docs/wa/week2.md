#What happen if all the moss of a city its remove?

##Hypnosis

The complete remove of moss in a city environment will demonstrate its productive attribute for the city in a completely brutal way.

##Methodology

We can understand its value for city ecosystem services buy calculating the benefit of moss.

We will do to demonstrate its productive attribute throw computation methodology, creating scenery’s using a time base model of moss absent with an environmental simulation.

We will simulate in this computation methodology two environmental city scenery; one in the present with full natural presence of moss and other in the future years after all the moss was remove. Comparing between them different topic’s, that are the moss productive attribute for the city, which are helping; pollution, humidity, insulation, green areas and citizen’s happiness.

###Moss Research

I realize that moss is present in many places in the city, is always around us, is a little pies of nature in the streets, building, parks, etc. It has many productive attributes, makes citizens happier, helps insulation buildings, creates green areas and fresh air.

The big problem of the today citizen’s is that they are not relicensing their good benefits kill them with chemicals.

Here are some examples of presence of moss in the city:
![](assets/m1.JPG)

![](assets/m2.JPG)

![](assets/m3.JPG)

![](assets/m4.JPG)

Here are some examples of good use of moss in the city:

![](assets/m7.jpg)

*Moss walls to create a more natural environmental in the city. By Chartier-Carbasson.*

![](assets/m6.jpg)

*Advertising with moss. Eco Art by CURB Agency.*

![](assets/m5.jpg)

*CityTree is a pop-up moss wall capable of consuming air pollution creating a small forest in the middle of the city. By Green City Solutions.*



##Notes

We all come from the same beginning.

All the resources of the world have the same principal elements, we are compound of atoms, many atoms create a molecule, a type of molecule is the DNA many of them creates a chromosome, many chromosome creates a cell that its the smallest structural and functional unit of an organism is the smallest unit of life. After cell the different resources take their own road with their special components.

The Big Ban was when nature was created after that the universe born, the atom was created and the life began. That way all the resources have the same principal elements all of them comes from the Big Ban.

If you see this in more detail in the food resources you can realize that most of them have the same components nutrients but in different, properties, types and proportions, being strong in some special nutrient.

This next infographic will demonstrate that different natural resources have the same principal nutrients:

![](assets/m8.jpg)

So in a limit environment resources we should not be focus in a specific recourse we should be open in different types of them because the nutrients or characteristics are not only in one natural recourse are in many and all of them have the same principal elements and began in the same place the Big Ban.
