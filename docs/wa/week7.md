#Looking inside the technology

##When you know how they are built you know how they work.

During this week we realize how technology really work so the best manner to see this is to open them. We took some old home appliances like and digital house phone, a kettle and a printer.

Realizing that they have many components of different shapes and types ones in different places of the object and others fit it on a green board. At first, you don’t really know why they are there and what is the function of them, but then we learn how to read all this information. Realizing that you can search for them because they have a special number and letter codes having the answer about their function and the realizing way they are there. See in then all these components now like a complete skeleton were all the pieces have their proper role.

Leering that all this components’ are also open for you to discover, buy and make whatever you want. You can be guided for his old electronics and design different types of home application useful for your necessities.

Looking haw things work:

![](assets/15.jpg)
*Digital Phone.*

![](assets/16.JPG)
*Home Printer*

##How the Internet works

The Internet is something that nowadays is hard to imagine without it. But how it works is a good question.

This infographic made by the graphic designer Abby Bennett explains in an easy and easy way how it works. Stating with the example when you search some web page, then you connect to your router or modem passing throw a firewall allowing and disallowing data, after that your ISP(Internet service provider like Vodafone, Orange, Movistar)transfer the data, then the browser search for the IP address that you are searching, then it finds the speediest way to reach the IP address, then it find the server IP address and connected to your computer and shows you the website.

![](assets/17.jpg)
*Graphic design of how Internet works.*

The most incredible thing is that all the Internet system it came from a wire connection, were they cables crossing Europe to America, the cities are connected throw cables, then the houses have their one little cables, it seems to be a digital system put it all came to form a physical connection.

They are companies that connect us to the network named like Inter Service Provider (ISP), like Vodafone, Movistar, Orange, but the big internet companies like Facebook, Google, Twitter also have ISP put they are different much bigger with bigger capacities faster connections, that join us to the network.

![](assets/19.jpg)
*Country statistic that can not live without Internet.*


![](assets/18.jpg)
*Percentage of population without Internet access.*

##Crating Machines

When you are looking to join the digital with the real world is the moment when you need to create some robot machines, designing an interaction between the world and the computer.


<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/uNkADHZStDE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

*Open-Source CNC Farming Machine controlled by an Arduino. Planting seeds and then watering them precisely.*


Nowadays the technologies have been improving more and more make it easier to create different types of robot opening the mind of the invention creating a more united world between the digital and the physical. This is because of the creation of microcontrollers hardware that is connected to a computer charging in him different orders in a machine language (code) and in this boards you connect sensors, motors, wires, lights, whatever that you what to interact or take information for the world and communicate to the computer.

30 years ago bring a camera computer like a webcam was very expensive now it is very cheap you can buy one for 5 E$. Now we have more opportunities

###A few pieces to create them:


**Hardware Boards or microcontroller**, is the intermediate between the computer and the interaction elements, is the element piece with it you cannot do anything.

![](assets/20.jpg)
*Raspberry Pi, is a type of Hardware Board, creating a computer power in a cheap way, with USB, HDMI and RAM memory. Like a little computer for simple things could be a WiFi router or home appliance.*

**Coding**, is the language that the hardware understand and is the manner were computer makes the orders for it.

![](assets/21.jpg)
*Coding in Arduino.*

**Flowchart code**, sometimes it is difficult to visualize the code information and began to create it so the best thing to do is start creating a flowchart of the code and help you start.

![](assets/22.png)

*Code Flowchart before coding a push button in Arduino.*

**Sensors**,they are two types.

Sensors, you can capture different types of inputs, air pressure, pollution, light, sound, temperature, etc, in a digital way. A human can not realize in a precise way different types of data so sensors are a very useful tool for it, like sound vibrations in a classroom.

Actuators, they have outputted date make something to the outside of the computer interacting with the real world like LED, LED RGB, Electromuments materials, Servo motors, and DC Motor.

![](assets/23.jpeg)
*Example of sensor, a city pollution sensor.*


##Workshop

Finally, to learn and put hands onto the world of the robotic machines we began a workshop in class to create one. The topic was to create an input or an output that interact in the classroom with our classmates.

Then, with my group we decided to create an input that it will be a "Mood Sensor", creating data throw different sensors and then mixing it will create a final result that would be the mood of the classmate in that specific time.   

The first step was to create the levels of mood that we what to identify, after a long discussion we realize that there are 6; relax, lacy, party, exited, work, focus.

After that, we began to look and observe what are the different actions of the classroom that define some type of mood, discovering this next inputs:

**Sound**, the different type of noise that the classroom makes an important data to know the mood of them like if it is very noise they could be excited, is they are not sound they are in a focus moment.

**Light**, the different types of intensities of the classroom lights tell you what they are doing, if the light is all on they could be doing some workshop class, is they are are only one on is a focus moment.

**Number of people**, we notice that it is different the sound or the use of the light in a classroom when it is full or it empty of people, like if there is no sound could be a focus moment but if there are no persons in the class it would be a wrong data. So we need to detect how much people are in the classroom and that mixed with the other two inputs.

###First Step

So we began to create this mood machine, creating at first the tree input separately in an Arduino hardware board coding it in Arduino software.


![](assets/1.JPG)
*The sound sensor connected to an Arduino.*

![](assets/2.jpg)
*The ultrasonic sensor connects to an Arduino, moving a DC motor when it identifies a movement like a first step.*

![](assets/3.JPG)
*Two ultrasonic sensors, one to count how many people comes in the classroom and the other one to count the ones that left it. But we have problems subtracting the data that one sensor.*


<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/axa5gsLWPDU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

*Testing light sensor data, quickly reporting if the classroom was having the light on or off.*


![](assets/4.JPG)
*Then began to connect all together in the same board and code, the sound sensor and the light sensor with a potentiometer to regulate better the data.*


Testing data input result of the light sensor and sound sensor. We decide to reduce the data of the sound in only focus (low sound), relax  (middle sound) and excited (high sound) as a first step because it was too complicated to control it, so having only 3 variables give it a bigger average and make it more precise.

Testing the sound sensor we also realize that the sound change every second is not a constant level, so we decide to count the highs picks of sound in 10 seconds so if they are more than 50 it will say excited, if more than 20 but less than 50 it will be a relaxing time and if it is lower than 21 it will be focus.

The light sensor will be also checking after 10 seconds if the light is on or off, so it will inform at the same time that the sound sensor but if the light does not change because that is an input less variable that the sound it will not inform it.


<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/maSfxuSJmPo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>


*Testing the light and sound sensor together. Counting the pick sound and the light levels showing the result after 10 seconds.*


###Second step

After we have all the sensor, the next step was to connect them to a database network were all the class computer machines input and output will be connected and giving their different data. So we need to change the board that at first was made in an Arduino now to an ESP8266 Board that has the capacity to have WiFi and sends to some local server.

![](assets/5.jpg)

*ESP8266 by a Chinese manufacturer Espressif Systems is a low-cost WiFi microchip with full TCP/IP stack and microcontroller capability.*

![](assets/6..JPG)
*My first step in sending a message, connecting the ESP8266, coded on my computer, to the local server of the classroom.*

![](assets/7.JPG)
*Data classroom base by and Raspberry PI.*

The next step was to connect the sensor to this microcontroller and send the data to the database. So we decided that the sound and light sensor will be in one ESP8266 because they can sensor in the same place and the motion sensor will be in other board in the door to realize if they are people going in.

![](assets/8.JPG)
*Light and the sound sensor connected to an ESP8266.*

![](assets/9.JPG)
*Finally, database receiving our information of sound and light, in the separated task because was easier to join it to an output and in number to create the same language between the different input and outputs.*

###Final step

We need to create a kind of cardboard box that contains this machine with the sensor, the board, and cables in a position that they don’t move take the correct data, all connected to a battery because they will be away from a plug.

![](assets/10.JPG)
*Light and sound box in a pillar of the classroom in the middle of it, perfect position to take the correct data.*

![](assets/11.JPG)
*Movement box, over furniture near the door entry, so it will check if people are coming into the classroom. Finally, we solve the problem of ultrasonic sensor changing it for an LDR that is a light dependent resistor.*  

Finally, we began to play connecting the input with some other output, trough a platform named Node-RED. Were you can see all the input and output connected to the server and see de the data in real time through your computer but connected to the same WiFi and then connect the input with the outputs.

![](assets/12.jpg)
*Our Node-Red database showing at real time all the data of all the classmate inputs.*


![](assets/13.jpg)
*Node-Red graphic visualization of the inputs.*


![](assets/14.jpg)
*Our input of movement sensor connected to this output of blowing a balloon, so when someone passes through the door the balloon will begin to grow.*
