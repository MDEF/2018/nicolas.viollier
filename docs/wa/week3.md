#03.Designing Process

During this week we need to convert with trash of the streets our classroom in a comfortable please to work throughout the year, covering all our needs like a space to have classes, to work in groups, to prototype, to read, to inform us, to discos and chill out.

During this work week I realize that design have different environmental factors and work stages that need to be respect and pass throw, not like a check list because designing is a creativity work and structuring it will create a bad work that do not flew up as expert designers says will not "go out of the box", but in different ways you need to pass throw this different steps and factors that I discovered to have a good design and team result.

##Work Stages

So the "Work Stages" will be in order chronological starting in what to design and finish in the final product. So this is divide en 3 stages in them it will be more sub stages:

**Research:**  
Moment when the team thinks carefully in the design request, the user, the space and the entire factor that are involved and haw to solve it. So this stage has 3 steps.  

 1. Observation of necessities; when you realize the different problems that the users have.
 ![](assets/D15.JPG)
 *Space design sketching.*
 2. Sketching; when you draw some first design ideas that can solve the problems discovered in the last step making more real the ideas and create a team discussion of what to make?  
  ![](assets/D11.JPG)
  *Drawing ours final ideas, the Coffee Lab.*
 3. Presentation; moment when the team presents to the order or client the observation,  the problems discovered and the design ideas through drawings and 3D models, these step make a discussion between them of what is the best solution what they are really needing.
  ![](assets/D13.jpg)
  ![](assets/D14.jpg)
*Renders Design of the Coffee Lab*

**Synthesize:**
After the presentation many thing changes, the team has a best vision of what the client wants and likes of your ideas this is the moment to design think in the real construction of it. So this stage has 4 step.

 1. Join ideas, after the presentation many thing can change and other could be approved so the team need to discus what they do what they change and how.
 2. Resume, all ways the team had many ideas but this is the moment to select and be focus in someone’s.  
 3. Material select, with what you will do it, digital, printed, wood, plastic, etc, they are many materials and selecting the correct is the first approach of how it will be.
  ![](assets/D16.JPG)
  *Selecting the best and useful recycled materials*
 4. Organize, with your team divide de differentness works, realize what are the skill of the members of skill and haw it could be use for a best result you can not make all distribute and trust in your team to have complete fabrication.

**Creation:**
The finally stage is to take the research and the synthesize and began to create the real design going through 4 final steps.

 1. Modeling, depending on the product that you what to make, is the moment to began to create the final blueprints or digital illustration or the 3D models, etc.
 ![](assets/D18.JPG)
 ![](assets/D19.JPG)
 *Design of the Coffee Lab table and the Arduino box.*
 2. Fabrication, when you make the real final design could be the print of the illustration, or the laser cut of wood, etc.
 ![](assets/D12.JPG)
 *CNC router making the tables shelves*
 3. Assemble, when you join all the parts, that have been recently create, together to make the product it could be to join the illustration prints to make a book or join the wood to make a table, etc.
 ![](assets/D17.JPG)
 ![](assets/D5.JPG)
 *Assemble the wood table legs and the top shelf.*
 4. Presentation, this is the final step when you show it to the world and give it with its details. Now it is not yours the design are the users the owners and will used at its on way.
 ![](assets/D20.jpg)
 *Presentation to the classmates ours final Coffee Lab*

##Environmental Factors

Also I realize that their are other type of stage that are not involve in the step by step of the creation they are involve in the ambient manners to work that I name like "Environmental Factors". This are focus in different things that need to be present and be use’s.

The environmental factors I defined like three and need to be use and the same time they are not chronological.

1.	**Team Work**
It is very important to be evolve all the team in the project design, they have different skills and the team need to take advantage of it, so you need to divide the work so you do not make the same work you trust in the others you make some revision appointment’s and make all the team help the different works to be improved hearing carefully the opinion of them and make a design of the mix of the ideas off all the team.
2.	**Schedule**
If you organizes before you start to work the time that you will be working it is better for all the team and the use of time is improved, the team knows that their are a final time,  so you work better and more focus, you don’t have lacy times but you have rest times.
3.	**Work space** it very important to make a place were you will be working during all the time, it were you have your research, your sketch, your prototypes, etc. Could be a simple tables in a classroom but it is important to be the same and make it you own space and the place were all the team have reunions and work together.
![](assets/D2.JPG)
*Ours works space during the whole week.*

###Final Product

So finally we present what we named the "Coffee Lab Compost" space were you can make your coffee, between the two tables using the holes to filter the coffee, or drink a tea taking the tea leaves form the plants in the shelves. It is all built of recycling materials designed in a organic way to create something with an a esthetic more natural and help the functionality of the space. Compose of a main table a bottom shelf to storage the different coffees and tea and two top shells to storage the cups and with big holes for the tea plants pots and.
With a compost system to compost the coffee and tea trash in a box and will create mushrooms that then you can eat them, the box will have an Arduino system to regular the compost.

*Final design presentation*
  ![](assets/D1.jpeg)
  ![](assets/D21.JPG)
  ![](assets/D8.JPG)
   ![](assets/D7.JPG)
  ![](assets/D9.JPG)
  ![](assets/D10.JPG)
