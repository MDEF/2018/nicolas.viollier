# Human Evolution

Jose Luis de Vicente teach us that, the human being is in a constant transformation going through different stages changing his manner to use this planet named Earth. One of them chronologically is;12 thousand years ago we start dominating the land, then 753 a.c. Rome was founded on the first great metropolis of humanity, then 1908 was created the first car model manufactured by massive production with a low cost, named Ford T "the car for everyone", then 1945 USA dropped the first atomic boom in Hiroshima, 2011 the Islamic terrorist named al-Qaeda make 4 terrorist attack to USA killing 2.996 people, then 2016 was declared the year with less ice in the world, at the same year United Kingdom make referendum, with a 52% of approbation, declaring that they will get out of European Union and at 2017 USA named Donald Trump their president with ideas of nationalism and deport immigrants.

![](assets/n9.jpg)
*Ford T industry production.*

![](assets/n12.jpg)
*Osama bin Laden leader of the terrorist organisation of the terrorist attack of the twin towers.*

**We are transforming this world in a competition of who is the strongest**, passing the year we are being less an less united making a more stressful place, making more damage to the Earth nature with big amounts of trash thrown every day to the ocean and CO2 in the air, quality of life is the worst and worst. We need to react and make changes, we have the tools we all need to use correctly.

![](assets/n16.JPG)
*Barcelona citizen manifestation because of climate change.*

## Looking for a happy future

Indy Johar architect and co-founder of project00, have a good point of what we need to do in this time where the world is exploding and we are more and more people but we are more and more lonely. He says that "the big revolution of our age is how we organize ourselves all the changes that we want to make can´t be made if we aren’t organized".

Democratized everything, he said were we are all working together not as competitors, good examples are organizations such as Wiki House, Open desk, Arduino and Open Source. The actual companies, need to use a commitment to open-source principles, with a democratic participation and transparency, creating an open network of companies, with massive contributors, where it is not based in one huge distributor they are many local distributors.

**"The Big revolution of the 21 century will not be the technology it will be about us and how we organized"**

![](assets/n8.jpg)
*Indy Johar idea of a democratized society.*

##Society in nowadays

Today citizen is very different how they act, how they manifest and how their social life is a comparison that 100 years ago. Now the people have two life and personality, one in the real world were you have an identity a social life-based in the physical interactive life and the other one in the digital world we were the interaction it’s through pixels in were you can play in many ways don’t have limits creating an identity that could be different than the one in real life were you can be whatever you want.

###Kusama moving Society

Throw a research’s of what is society searching now and what are the excitement’s of today citizen’s. I saw this article of Tim Adams in The Guardian that talks abut Yayoi Kusama who is the favorite artist of the world (goo.gl/pe645G). This is an excellent example of what society is looking for. In the past five year’s more than 5 million people have seen her work in all the world, Los Angeles Broad museum in one afternoon sold 90.00 tickets that cost 25US$.

The society is searching for places, entertainment’s and hobby’s that makes them interact between this two worlds the real and the digital. Where they can show in the digital world different social media their real-life interactions’ Kusma exhibition’s is a perfect place to do it.

![](assets/n7.png)
*Kusama art-world phenomenon in the age of the selfie.*

###Transhumans

Actually, the technology of robots is being more and more improved, where they are machines that can help us in almost every things having different skills that the humans could not do like ultraviolet vision ore magnetic connections.

So throw an article of The Guardian of also Tim Adams he shows us a new side of society were humans what to have this robots skills making a new species names "Transhumans". Bodyhakers is the knowledge of transhuman were they are experimenting in different ways searching different thing one for art, other for medicine and other for countercultures.

David Vintiner, a British photographer, has been following this subculture of transhuman for extending their life another playing with their body’s as art and other to make permanent changes to human conditions. Here they are some David picture works were he make us now the real impact of a transhuman.

Here they are some of the pictures taken by David Vintiner:

![](assets/n2.jpg)
*A women picking a spoon using a magnetic implant.*

![](assets/n3.jpg)
*A boy that lost his right arm.*

![](assets/n4.jpg)
*Neil Harbisson, famous man that can't see the colors so he invented a robot that transforms de colores in sound.*

![](assets/n5.jpg)
*He lost his eye in a childhood accident, now he had a camera and transmitter fitted into his eye.*

![](assets/n1.jpg)
*Eyesect is a helmet with mounted cameras letting you see the world in a different way than humans.*

##Was the design role in this future age?

Pau Alsina makes us realize that design can change the act of society, design can be part of political decisions. The designer creates object’s that can make society interact in a different way, and example glasses make you see better and interact differently in a classroom if you don’t have it. The designer has the capacity to take technology and transformed it solving necessities. Technology is all the human creation that doesn’t come to form nature, a riding a horse is a technology created a long time ago but is a creation of human to solve a transport necessity.

**The power of the organization can make powerful changes.**

The designer has the role to take all this technology and make it useful. Using it to organize us in a democratic world. We have now the media to be all connected in an easy way, 100 years ago citizen would dream to have instant news as we have now, we need to use them in a positive way making interactions between the real and the digital worlds.

Design need to promote and move society in a collaborative way of life not only the manufacture’s the users also need to be involved. Were society think in the others, if a person bay something that was really bad he writes in some social media that do not bay it helping the other to not make the same mistake that he made, in public spaces think in the others if you drop a bubble gum in the floor someone will step it, etc.

###Sustainable Development Goals

In the framework of the smart city week of Barcelona organized by the Ayuntamiento of Barcelona, United Nations Sustainable Development Goals.
Participate in different politics of the Govern of Spain, the Generalitat of Cataluña and the Ayuntamiento of Barcelona. Were the Sustainable Development Goals (SDG) are a set of 17 global objective’s to eradicate poverty, protect the planet and ensure prosperity for all, with the objective that in 2030 will be all solved.

![](assets/n10.png)
*Sustainable Development Goals, United Nations.*

So they talk about how the Govern, the states, the Municipality’s and the citizens can be involved and make throw this United Nations sustainable objective’s. Detailing us that they are looking throw a Barcelona Safe but not only more police in the street, but we also need 0% of poverty and hunger,  improving health and education, clean water, gender equality, clean energy, work quality, industry innovation, responsible consumption, and production. Creating a safe smart city achieving the objectives of the agenda 2030 of the United Nations.

![](assets/n15.JPG)
*Gerardo Pisarello, first counselor of the mayor of the Ayuntamiento of Barcelona and Cristina Monge director of ECODES, in the talk of the smart city week.*

They also talk to us that we need to think and act in a local and global way, where the big city’s of the world need to go in the same way of working together. Were the private's companies are joining with the Universities. Making alliance betting the Govern, the University’s and the privates


###Good actual example’s


![](assets/n14.JPG)
Trush can recolector system in Barcelona.


![](assets/n13.JPG)
Coffee machines with a transparent system of creation.


![](assets/n17.JPG)
Toys made of 100% recycled plastics.


![](assets/n11.jpeg)
Algramo, a chilian brand of store food with a system of retornable packaging.


![](assets/n6.jpeg)
Kyle McDonald, code artist creator of the interactive digital music maker the NSynth Super.

##My Vision

**Goal:**
Use technologies to make a democratic and sustainable world.

**Direction:**
The technologies are all made, we need to search and use them in a positive way where they can help joining and organizing us looking forward to a more sustainable and natural world.

**Business:**
Create a company that looks the future in a positive way using new impact technologies as a tool not to change our interactive social natural life but to improves people life in a consensus manner.
