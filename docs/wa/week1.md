#Production Cycle

After a week of observation and research during this Bootcamp I observe and center my thoughts in Poblenou neighborhood in the district of San Martin in Barcelona.

![](assets/ee3.jpg)
*Super Block project in Poblenou*

A place that is really near the center of the City but do not have the characteristics of it do not have the same architecture, tourism, urban order, amount of people it feels a different air. You cross the bridge that divide the district of San Marti with Ciutat Vella and you enter in Poblenou were all changes, you see industries, cars mechanics, families, universities, mix of new and old buildings, no hotels and do not have car traffic.

![](assets/ee4.jpg)
*Poblenou is a place where old building have being intervene in correct way*

![](assets/ee5.jpg)
*Mother with her son learning how to fix a bike in Vés en Bici company*

So during my observations I realize that are a few identities in this place that are generating indirectly day for day a new movement that is the characteristic of Poblenou.  These identities are compound of people, objects and places.


##These Identities are:

###Industries:
I defined them like a medium innovative industry lab, because they are in the middle of a little hand craft industry and a big industry like a Amazon warehouse and because they are trying to create different things, joining design, technologies and new materials.  
###Universities:
There are different little institutes or universities with a very personalized manner to teach like ARTI, BAU, IAAC and Barcelona Academy of Arts, that are going trough the same convictions.
###Recyclers:
They are in most immigrants that go around the neighborhood in the days were the big trash collecting all kinds of materials generating a new type of job.
###Artist:
The are many art workshops, art gallerys and studios of design and architecture around Poblenou that are creating many interesting stuff for all the city with something especial.

![](assets/03.jpg)
*Recycled sculpture workshop of Poblenou, named Pentaculo Escultores*

![](assets/ee2.jpg)
*Transfodesign, a cowork and workshop for recycle projects*

![](assets/ee.jpg)
*Indissoluble itinerant design studio project named Brain for a fair in Madrid*

These 4 identities described that are from different backgrounds, I realize, are generating indirectly a circular production that is described in the this infography:

![](assets/02.jpg)

This indirectly circular production make in Poblenou a place were different identitys in an informal way generates a type of life were all of them help together, creating a land with their one rules and not government rules.
![](assets/01.jpg)

##Idea
###How to create a more direct community? Could the recycler can help the universitys? Designer can help industries?

Taking this production connection in a kind of network help were different identities can shear their processing problems and generate a different manner of think doing their work and think haw to make better this cycle and making this community more powerful.
