#Material Driven Design

![](assets/m_2_47.JPG)

##Learning about material

We spent a whole day in Valladaura IAAC campus, self-sufficient labs. Walking through the Valladura forest learning about how natural raw materials are made, their qualities, their differences and all the wealth that you can take from them.

Finally, we needed to pick a log from the forest and learn about the material through making a wooden spoon. So after observing it and design planning, we began to make the shape, carving it with special tools.


![](assets/w_1_6.jpg)

*My initial log in where I will create my spoon.*

![](assets/w_1_7.jpg)

*First you start with the ax.*

![](assets/w_1_8.jpg)

*After carving with the ax you cut the log to began to make a more curve shape.*

![](assets/w_1_9.jpg)

*To make the hole of the spoon you burn it with charcoal and then I take out all the burned wood.*

![](assets/w_1_10.jpg)

*Finally my spoon, of laurel wood.*

![](assets/w_1_11.jpg)

*All the spoons made in the class.*

###Reflection:

- The knowledge of craftsmen to easily and perfectly create a wooden spoon.
- How nature surprises you finding different things that aren’t in your plan and make you change your ideas
manner in which you work, like the force required to carve wood knots.
- Seeing and feeling the materials you can learn more about them, their characteristics, how they grow and how to work with it, etc.


##Searching for your material

After learning to abut engaging with a material like the last lesson in Valladaura we need to search for biomass, that is a biodegradable raw material.

I am a lover of eating meat but the left overs, the bones, sadly I need to throw into the trashcan, as I am unable currently here in Barcelona to compost. Also the butcher when I buy some steaks, he takes the bones and throws them to the trashcan. I observed this structural material of the cow, pork or chicken, and see it as something powerful that we can do something with.

I then decided to look for the state of the art of bones, going to different supermarkets, butchers and markets. Learning through asking people who work with meat and observing them. They collect a big amount of bones, very strong ones like the ones of the legs and the spines, especially in pork and bovine.

Butchers sell bones for making soup but this not a big business. It is a way to get rid of this calcium material or make some money in winter. I was obtaining 3kg of cow bones for free, and if I wanted more I could have more easily. I only needed to arrange with them the day that they would cut the animal so they can leave me the bones for me.

**Material selected: Animal Bones**

![](assets/w_1_13.png)

*Bones have a hard tissue.*

![](assets/w_1_12.jpg)

*Crafts made by cow bones.*

Skeletal Structure of all living vertebrates.

Composite material, 70% inorganic and 30% organic
Forming a strong, lightweight and hard tissue.

Inorganic part is:
-  Mineral calcium hydroxyapatite, provides compressional strength and rigidity.

The organic part is:
-  Collagen, elastic protein that provides pliancy and overall fracture resistance.
Bone has a high compressive strength of 179MPa with good elasticity.

Created by osteoblasts cells secreting collagen fibers and ground substance (amorphous gel).

###History of human usability with bones:

Bones, for creating tools were used by Neanderthal people 1.5 million years ago. Creating hunting tools like spears and arrows, fishhooks, knives and domestic products such as needles, pendants, and combs.

![](assets/m_1_4.jpg)
*Neanderthal bones knives.*

Bones were also essential, 40,000 years ago, as part of musical instruments.

![](assets/m_1_5.jpg)
*Old flute made form a bone.*

###Problem:

**Fossilization**

They have the characteristic to fossilize and remain preserved for millions of years. Finding them hundreds of years in the future in the dumps with the plastic wrappings and the tin cans.

###Provider:

Butchers.

###Actual use

1. Fertilizer

Bone flour, it is an organic fertilizer that is a good source of phosphorus while for animals, it supplies mainly proteins.

![](assets/m_1_2.jpg)
*Bone fertilizer.*

2. Food supplement for animals.

To beef up the constitution of bones and skeleton with the provision of additional calcium. Perfect to complement the normal diet of dogs and cats with minerals and trace elements.

Analytical components:
- 100% Caw organic
- Crude protein 0.2%
- Crude fat 0.3%
- Crude fiber 0.3%
- Crude ash 81.4%
- Calcium 35.7%
- Phosphorus 25.6%
- Magnesium 0.022%
- Sodium 0.016%
Source: https://www.zooplus.es

![](assets/m_1_3.jpg)

*Pet bone supplement.*

3. Bone soup.

In some butchery, they sell the leftover bones from the cut of the meat to make a strong soup. After talking with some butchers I realize that isn’t big business for them is to make some money with their waste in winter time selling some of them very cheap and others throwing it to the trash.

![](assets/m_2_1.JPG)
*This is a very good soup bone made Spanish Iberico Ham bone.*

4. Bone Glue
It is an organic glue, made by the hydrolysis of bone collagen the protein substance contained in the tissues of living organisms. Collagen is an insoluble fibrous protein that occurs in vertebrates as the primary constituent of connective tissue fibrils and in bones and yields gelatin and glue on prolonged heating with water.
Source: https://www.connect.ecuad.ca

5. Organic implant
Through the extraction of Hydroxyapatite for bones you can create a biomaterial that helps the resemble and structure of other bones. Replacing the non-organic Titanium mineral that is actually used for medical implants.
Source: http://penerbit.uthm.edu.my/

![](assets/m_1_1.jpg)

*Using hydroxypatite like a implant for humans bones.*

##Making My Bone Material

After knowing some of the actual applications of bones I began to search about its chemical components looking for creative and innovative material. Removing it from it´s normal aesthetic using it´s principal components and transforming it in something new that surprises you that come from animal bones.

I found reading different scientific papers that bones are constituent of an inorganic phosphate mineral named Hydroxyapatite. It is a calcium biomaterial that has many applications like bioactive osteoconductive, non-inflammatory, biocompatible, non-toxic and non-immunogenic.

It is a bioceramic one of the most similar material of human calcium. Making it useful for medical applications like teeth and bone tissues implant replacing titanium that is an artificial material that that is not compatible with human bones. Being compatible with human bones it can have excellent functionalities like the restoration of skull defects, restructure bone defects, creating bone tissue engineering scaffolds, removal of heavy metals and drugs delivery.

Actually, this calcium bioceramic material it's used for different clinical applications mostly the most common is in teeth implants where they are some innovations 3dprinting it and making different experiment looking for the best manner to used and join it.

![](assets/m_2_2.png)

*A 3D print specific bone graft for medical use made of Hydroxyapatite.*

![](assets/m_2_3.png)

*Odontology coating instruments made of Hydroxyapatite bone calcium.*    

These are the two scientific papers that were my guides to get in the Hydroxyapatite bioceramic material:

1. [Natural Hydroxyapatite Extracted From Bovine Bone](assets/Paper1.pdf)

2. [Preparation of Natural Hydroxyapatite from Bovine Femur Bones Using Calcination at Various Temperatures](assets/Paper2.pdf)

3. [Synthesis and Characterization of Jellified Composites from Bovine Bone-Derived Hydroxyapatite and Starch as Precursors for Robocasting](assets/Paper3.pdf)

##Creating my Calcium material

Taking advantage of being structural, light, natural and a pure calcium mineral. My ideas were to create a bioceramic made of pure calcium from animal bones looking to create a material that can have applications in the medical area but also in construction area like a new type of cement.

Searching to create a circular cycle using all the waste of bones that are thrown on the same role that it was created, that was for structural functions in animals. Now structuring a human body, an object, a building, etc.

###Experimenting to create a hydroxyapatite powder

![](assets/m_2_61.JPG)

To create the Hydroxyapatite I follow the instructions of how to extract the bone calcium from bovine bones for the two papers that I mentioned before.

So I proceed to create this calcium material with some bovine bones that local butchers give me.

The most difficult process was to create a very fine bone flower. This is the next steps that I did to create it:

**First Step** Boil the bones.
![](assets/m_2_4.jpg)
*After cutting the bones I boil them for 3 hours. Losing it´s hardness and letting go of it´s marrow and fat.*

![](assets/m_2_5.jpg)
*Bones after being boiled*

**Second Step** Wash and clean the bones.
![](assets/m_2_6.jpg)
*I washed the bones for a whole day in distilled water and then with a knife I cleaned it removing all the cartilage and fat.*

**Fourth Step** Dry the bones in an oven at 230˚C.
![](assets/m_2_48.JPG)
*I wanted to dry them for 3 hours but before they were 1 hour in it they began to get burnt so I need to turn the oven off. Observing the bones that were starting to be charred wasn’t the idea.*

**Fifth Step** Milling the bones until you create a soft powered similar to flour.

*This step was difficult because I don’t have the proper implements, only a cooking blender but I would break it if I used it with bones. So I decide to do it in different stages.*

First to create the smallest pieces I smashed them with a hammer, then I wanted to try two options that create smaller pieces near to some type of powder, one was using the precious plastic machine an the other is the meat grinder.

![](assets/m_2_8.jpg)
Using the precious plastic machine, it was good but it doesn’t make the pieces very small and also I realize that not all the bone was really dry inside and clean. Some of them still had some fat or marrow.

![](assets/m_2_7.jpg)
The idea was to create a power of pure bone, not other things so I put in a bowl the bones that look to be the driest and without fat, and in other the ones that were with fat that were the least dry. Then with the meat grinder, I began to mill the dry bones. Realizing that the **bones weren’t so hard** as I thought. I need to insert the bones several times to finally have a sand like powder.

![](assets/m_2_10.jpg)
Then finally to create a soft powder I strained it carefully, the result being a green flour.

![](assets/m_2_12.jpg)
**Having my bone flour finally.**
The bone outcome was green however, and I felt that it was still a little bit moist. I took two samples, one being put for one hour in the oven at 230˚C and the other one for 2 hours, completing the drying process.  
![](assets/m_2_49.JPG)
Having finally my samples of milled and dry bone powder with their different colors because their different drying time.

###Using industrial bone flour

Also, I want to compare the outcomes from the bone flour that I made with the one that you can buy in the market. So I bought on Amazon a bone flour fertilizer made from bovine bones.

![](assets/m_2_11.jpg)

*This is a bone flour that I bought named "Bone Meal", used to fertilized plants giving the phosphorus and nitrogen mineral that they need to grow.*   

The Bone Meal didn’t come properly milled so I needed to strain it having finally a pure soft powder similar to the one that I made. Also the result was about something that looks less pure.*

###Calcination process

Finally having a pure and soft bone powder samples the last process to create my raw material is done after doing calcination temperature process.

*Calcination is a process of heating a substance under controlled temperature and in a controlled environment. This process is known to improve the chroma, tinctorial strength, pigmentary texture, weather stability, light-fastness and thermal stability of pigment whereas it has an adverse effect on the dispersibility of pigments.* Source: ScienceDirect

When you pass throw bones in a calcination process they became free for any fat and protein having an outcome of a pure natural mineral calcium Hydroxyapatite.

To calcine bones, an obtained pure mineral calcium the recommendations were to do it between 700 and 1100 ˚C for 3 hours.

![](assets/m_2_14.JPG)
*This is the furnace that I use to make calcination process that can reach until 1100˚C the oven that I use to dry the bone only reaches the 230˚C that wasn’t enough to calcine the bones. I manage to bring it from Valladaura IAAC building down to IAAC main building, where we were working in the materials; they use it for clay cooking. Deciding that I would do it in 900˚C because is the media temperature for 3 hours.*

###First bone calcination

**Lesson 1. Find a proper bole**
![](assets/m_2_15.jpg)
*The first problem was that I haven’t had a proper pot to leave the bone powder able to resist the high temperatures. So I put a brick that has holes where I live the different sample to priced in the calcination process.*


**Lesson 2. Measure the time**
![](assets/m_2_16.jpg)
Measuring the time is something very important because the bones need to be 3 hours in 900˚C but the furnace spends the time to reach that temperature and also need time after the 3 hours to cool down. So I measure how much time it takes to reach the 900˚C so the next times I can leave it to allow and also I measure time cooling down.

![](assets/m_2_17.JPG)
*Here you can see through the chimney of the furnace how hot is the temperature in it.*

#### Final result

![](assets/m_2_18.jpg)
*This is the result of the calcination process of my different samples. Where you can see that the bones powders are now more white and light, put also the bones that comes from the "Bone Meal" where less pure, green and with more ash. But the one that I made that was dryed more time in the oven was the whites and more pure*

Having finally:
**Pure calcium material**
![](assets/m_2_65.JPG)
*The outcome was very good and interesting powder. The material was very similar to seafood shells and eggshells.

![](assets/m_2_19.jpg)
*This is how it looks the different hydroxyapatite just created. Observing that the different extraction processes generate differents outcomes.*

####Correct process of calcination

After my first time making the calcination process I realize that when you put bigger peace’s of bones in the furnace the outcome was that they louse there hardness. So creating a pure powder of it was much easier and I do not need to spend time smashing bones to create little pieces.

Also that the different bones coming from different cows are different ones are more hard, heavy with a little tissue and others are more weak, light, with whole tissue structure.

Knowing that I began to search in a calcination process more effective for all the types of bones.

**Finding that the principal step was to put in the furnace instead of a bone powder pieces of bones.**

![](assets/m_2_24.jpg)
*This is the bone pieces after being cleaned and boil, before going to the calcination process. They were so hard that it was impossible to smash them with a hammer. Also, I try to milled but the material was so hard that I couldn't.*

![](assets/m_2_20.jpg)
*This the result of bone pieces calcinated.*

![](assets/m_2_66.jpg)
*Bone tissue after being calcinated.*

![](assets/m_2_21.jpg)
*The bone pieces calcinated where easily milled after.*

![](assets/m_2_22.jpg)
*Always you need to strain it.*

![](assets/m_2_23.jpg)
*Having finally my calcium powder easily made in big amount.*


##Creating a 3D shape

![](assets/m_2_61.JPG)

Know was the moment that I have finally my raw material a pure mineral hydroxyapatite calcium. So I needed to **start the search for functionalities, experimenting how it reacts in different shapes and situations and find its properties.**

The idea was to find a way to join my raw material with a processor with some natural binder. To be able to create shapes making it a functional material, with the possibility because is a soft, light and strong powder to create whatever you want.

###My different experiment’s

I went through testing with different process and blinders looking to create a new type of bioceramic cement made of calcium bones. Because the properties that the calcium bones have is similar to the limestone mineral that is used to create the actual cement.

**Way not in the future build house with bone cement?**

1.Water experiment

I test how it reacts boiling with only water looking if it dissolves, joins or change in some way.

![](assets/m_2_26.jpg)
*Having bad outcomes the calcium was never dissolved and it does not react with water. Was very similar to mix water with sand. When it is wet it creates different shapes and you can play with it put it is dry it all over the shape it is easily disarmed.*


2.Citric Experiment

Researching the industrial manner to bind hydroxyapatite calcium one of the manners was with a strong change of pH. So I try to see how it react with juice from a lemon.

![](assets/m_2_25.gif)
![](assets/m_2_27.jpg)
*At first the reaction was better ii reacts creating like dough mixing the liquid with the powder. But then I wait for two days to dray and the outcome wasn’t good it was the same powder but now instead of white was orange.*

![](assets/m_2_64.JPG)
*Liquid experiment mixing lemon juice with calcium changing it conssitence.*

4.Laser Cut
Another industrial method to join calcium powder was with a strong change of temperature. So I tested what happens if burn hydroxyapatite with the laser cutter.

![](assets/m_2_28.jpg)
![](assets/m_2_29.jpg)
*So I tested in the laser cut, setting its more powerful cut in a slow speed. Having an excellent outcome, where the calcium had a chemical reaction of crystallization, melting and then solidified together making a 3D shape.*

It is blowing mind processes where you can create with a 2D process because the laser cut work only with XY direction, a 3D shape. Making me think that you can create whatever shape you want. But the problem that the laser cut of Fab Lab wasn’t made to cut in the powerful intensity needed two melt the calcium, they let me only to make a little test if carry on doing it I would break it. So sadly wasn’t able to preside with this exiting process.  

4.Bone Cartilage

I was a little upset because was very difficult to find a correct binder because the calcium was too difficult to dissolve and join. Is like a type of sand that always get out of the glue that you will put and whatever liquid that you will put in will never liquefy.

![](assets/m_2_50.jpg)
*Then I saw this cartilage, that I have extracted from the bones and then leave it dry, realize that was all sow a very powerful material that could work as a binder.*

![](assets/m_2_31.jpg)
*Testing the hardness of the cartilage. I hammered it on a plywood board and it didn’t break also being so strong that it made a hole in the wood.*

Realizing then that this cartilage was a type of the industrial bone glue that wasn’t extracted in the correct way.

Making me think:

**Way not developing a material that all its components come from the same origin?**

Imagine brick of calcium from bones binder with glue form the same bone.**


###Experimenting with bone glue

After this conclusion, my focus was to develop a material mixing hydroxyapatite calcium with bone glue creating a whole composite material from the same origin.

![](assets/m_2_36.jpg)
*This is the bone glue that I will use.*

Starting to experiment with it searching to fund the correct amount of each component’s, and the methods to join the two. Always trying to use the less amount of glue to don’t lose the proprieties of the calcium. Using the industrial bone glue because to learn to extract correctly the bone glue will make me spend too much time.

1.Boiling it together

![](assets/m_2_34.jpg)

First I tried in many ways to melt the glue mixed with calcium powder in boiled water. Until the water has been evaporated in the pan. But the outcome was never good after the mix wasn’t joined correctly having always the same calcium powder with some separate pieces of bone glue.

![](assets/m_2_60.jpg)
*This are my bad experiment outcomes boiling the glue and the powder together.*

2.Together in bain-marie
![](assets/m_2_32.jpg)
![](assets/m_2_51.jpg)

Then I thought that water was the problem. So I try to join them mixing the bone glue and calcium powder out of the water in a bain-marie. But wasn’t also good because the bone glue in some moment began to be very sticky but no so liquid and was difficult to control and mix with the calcium.

**The Best process**
3.Boiling the bone glue alone

Finally, I get to the correct process. Realizing that the correct way is to work the components separated. Melting first the glue in hot water until almost all the water was evaporated and then having the hydroxyapatite powder in another bowl. Where you then leave the melted liquid glue and mix it together and leave it dry.

![](assets/m_2_35.jpg)
*This is the bone glue all dissolved in hot water.*


##Hydroxyapatite 3D Shapes

Having the correct processes wasn’t the only thing that you need to know I also discover after much messy experiment that was very important to know the proportions of the element. So I began to test measuring the quantities of the different components of the material.

After start measuring I began to have a better result and more controlled helping me to make a better conclusion of them that they were useful for the next experiments.

![](assets/m_2_52.jpg)
![](assets/m_2_53.jpg)
*My first 3D shape, where I have to bind the glue with the calcium. But with the problem that I do not take measures so I wasn’t able to make conclusions, make it again and also looks to have many glue that wasn’t the idea to take all the calcium properties.*

###Finally 3D shapes outcomes

**My Bioceramic Cement Material**

- Made 100% of bovine bones:

- 90% Inorganic Hydroxyapatite Calcium Mineral.

- 10% Organic binder for collagen.

After different precise experiment taking measures, making conclusions, leaving it dry the correct time. I have a good result in having 3D shapes.

Experiment 1:
Mold: 3D printed.

![](assets/m_2_54.jpg)
![](assets/m_2_55.jpg)
*Having after 2 weeks of drying an outcome of a cylinder that makes me dream about all the functionalities that could have this material. Being very hard and light, it could be useful to any construction material be then useful for agriculture because is rich in minerals for plants.*

![](assets/m_2_37.jpg)
*This is the 3D printed mold that was used. Inserting in it also in a plastic laser cut stick to help then take the shape. But the shape that was so hard that they weren’t useful.*


Experiment 2:
Mold: aluminum experiment plate.

![](assets/m_2_56.jpg)
![](assets/m_2_57.jpg)
![](assets/m_2_58.jpg)
*Also after two-week drying, I have an outcome exactly of the mold shape, but it breaks after. Having a soft and hard shape with some bubble that makes me think what if I could put air in it and recreate the bone tissue making creating better structural material using less.*

##Recipe Bone Bioceramic Cement

![](assets/m_2_63.JPG)
After all my process of experiment with bones, this is my final recipe of the material.

###Ingredients:
- Cut Bovine bones, in butchers they will easily cut it for you.
- Bone glue in pearl.
- Water

###Instructions:

![](assets/m_2_38.jpg)
1. Boil the bones.
For 3 hours leave the bones full of water boiling. They lost their gardens and all their different components like fat, cartilage and meet.

![](assets/m_2_39.jpg)
2. Clean the bones.
With a knife carefully clean all the residual material and marrow.

![](assets/m_2_40.jpg)
3. Wash the bones.
Full a bowl with distilled water then washes the bones in it and then leave the bones in it for a whole day.

![](assets/m_2_59.jpg)
4. Dry the bones.
For 2 hours in 180˚C dry the bones in an oven take out all the water form them.

![](assets/m_2_41.jpg)
5. Smash the bones.
Know that the bones have lost all their hardness put them inside a fabric bag and smash them until they are little pieces like the ones in the picture.

![](assets/m_2_42.jpg)
6. Calcination process.
In a clay bowl, that resists more than 900˚C, full the smash bones and put it in a furnace. Then calcined at 900˚C for 3 hours with a heating and cooling rate of 10 ˚C/min.   

![](assets/m_2_43.JPG)
7. Mill the calcined bones.
With manual meat, miller tool mills the calcinated bones until you have a soft powder similar to sand.

![](assets/m_2_44.JPG)
8. Strain the calcinated powder.
To have a pure and soft powder staining it will help to take out all the big pieces that weren’t milled.

![](assets/m_2_37.JPG)
9.Full the mold.
Create a mold that is made of two pieces and it is soft inside. Then weight the mold, then fill it with the bone calcinated powder and after weight it again. Taking note about the weight of the powder that fits inside.

![](assets/m_2_45.jpg)
10. Melt the bone glue.
Boil 3 more parts of the amount of powder and when it began to boil ad 1/10 parts of the amount of calcinated powder of bone glue. (ex. if the powder is 20g you need to boil 60ml of water in 2g of bone glue) Mix it constantly so the pearl of the glue began to be dissolved.

![](assets/m_2_46.jpg)
11.Mix the composite
When it is almost all the water evaporated and the liquid feels thicker. Take the pan out of the stove and deposit the liquid in the mold that has in the powder. Mix it until all the powder is wet and in contact with the liquid glue.


12. Let it dry
For 5 days let it dry at the sun or in a room but with room temperature without putting any extra heating. Then you open the mold and the bone calcium material will be done.


##Reflections

![](assets/m_2_67.jpg)
*My final presentation display of experiments*


- Measuring is important.
After I began to measure all my experiment the outcome became to be at the next level and was much easier to get better results.

- The material change in time.
When I present my material I thought I would never have a 3D shape but then after two weeks I cheek my experiments and some of them have changed and dry perfectly having happily some 3D shapes.

- Label the experiments.
Is important to label all to what they are because they could change in the time at it would and you would forget what was the recipe.

- The material reacts in different ways.
It surprises you and reacts much time different from what you thought.

- Is not a problem to copy.
Copy experiments are the first processes of making then you learn about them and make conclusions to began to make something new.

- Stop researching,
began to make. In a moment you need to stop reading an began to make, feel the material and discover what outcomes it can give you.

- Not common useful biomass.
Bones are a starting point of a common useful material that is commonly thrown to the trash. Imagine the number of powerful biomasses that it is wasted daily.
