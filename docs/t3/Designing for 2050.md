#Designing for the 2050

Design for the 2050 was course led by
[Andres Colmenares](https://www.internetagemedia.com/talks/andres-colmenares) , director of
[IAM](https://www.internetagemedia.com/) .

The idea of the curse was to create a full series, named Prismatic Minds, of positive future scenarios of the 2050, based in Heterotopia narrative. As an exercise to think about future scenarios.
The class was divided into groups where each of them needs to create one episode with different themes and choosing a specific city of the world, them, and object and a conflict.

##Prismatic Minds Episode

Here are the details the episode developed with my group.

**Tittle: Floating Point**

*  Theme: ID
*  Object: Totem & Senses
*  Conflict: Hyperreality vs. Environment
*  City: Lagos - Nigeria

Lagos, Nigeria situation, under sea level because of climate change. Dynamic Venice of 2050. Ebb & Flow every day. Parts of cities will be flooded every night. City and buildings are floating. Artificial Mountains built around the city. Flooded areas are to avoid for people living in the mountains.

**Main Character: Amadia** (the lightning spirit) - 31 years old. middle class. educated biologist. Works at "Mycocapsul Inc.“ and Lives on a mountain.


**Conflict:**
Once she has to go to the floating areas of the Transients (people living there - like the invisible). Siren sound signals that she has to go back before the area is flooded. Can’t go back because her ID is not working to cross the bridge. She has to stay in this area. ITs the first time that she sees how this area is flooded and how the people live during the evening/night. She comes in contact with the smell, physical objects, cooking and realises how unattached she is from her cultural Identity. She wants to conserve the smell so tries to catch in a can (totem) and bring it home on the next day.

**Story**

*It is a sunny morning of May 2050, in Lagos, Nigeria. Climate change impacted daily flooding periods. The city has a population of more than 32 million. So, adaptations on the infrastructure were needed. Floating areas were created, which needed to adapt 1 meter of water level rise. Also, artificial mountains were built, where the more privileged and wealthy people live. The flooded areas are avoided by them. They do not know about the dynamics that happen there during the night. During the day, some interactions still occur, and Amadia, a tech biologist, had some business to do in one of these floating areas that day...*

*Amadia is a Tech Biologist, 31 years old, from the middle class. She works in a Myco-capsule company that trains mushrooms to clean the earth contamination. Remediating toxins from human pollution.*  

*Amadia went to Lagos floating area, with a size of 4.3 square kilometers, because she needed to pick up some samples of a special toxic present in these lands.To get in the floating area she needs to cross a “Double Bridge”. Where she shows her ID card and then automatically the floors move her to the other side.
After collecting the samples she needs, she is ready to go back to the mountain. She heads to the bridge. But after passing her ID, the bridge isn’t moving. She gets nervous, but there is no one around to help her. It is only a machine. And it is getting dark…*

*She realizes that she will be not able to cross and starts to feel unsafe there. She is walking around desperately. Suddenly, in the middle of the darkness, she sees a small light. She gets attracted by that, walking towards a weird construction.She sees some people, that were not the typical she knows. You wouldn’t see in the place there, during the evening or night. They were getting organized to start one activity.*

*She observes that they were setting up a space, a temporary space. People were making objects, cooking - activities that Amadia never saw before. In her society, everything is made digitally. People never manipulated or changed the shape of things manually. Handcraft was never seen.*

*She gets fascinated by this and realizes how much she lost connection to a cultural identity from her own city. The smell, the feeling, the social gathering… Enchanted by the smell of the Ogbono soup, she finds a little can on the floor and try to capture that smell to take it home.*

*She spends the night with that community. When the sun starts to rise and it is time to go back to the mountain, she realises that the can, her totem, is not going to last… Maybe she will come back later that night.*

**Episode:**

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/_SvON89JxwM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

##Reflection

The future is something uncertain, with many changes that will supposedly create new manners of living. The curse helps me imagine the future beyond the typical approach of the future like, automatization, AI, robots, internet everywhere, connectivity, etc, it makes me imagine futures mind-blowing that cross boundaries of what we can dream know. Where the power will be mixing human intelligence with the developed technologies, of that time, creating a world of collective positivists, connectivity with nature, shared happiness, and much more. Because human cannot stop being humans, namely in the future they will not change their natural habits like eat, speak, drink, not because they are machines that make the world easier they will spot working or studying. Humans at that time will take the approach of it and work together to humanize scenarios.

![](assets/t3_2_1.jpg)

*An example of future scenarios mixing human habits with technologies. Source: www.internetagemedia.com.*
