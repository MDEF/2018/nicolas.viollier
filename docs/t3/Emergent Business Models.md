#Emergent Business Model

Emergent Business curse, led by Javier Creus and Valeria Righi from
[Ideas for Change](https://www.ideasforchange.com/)
. Was a moment to put in the grown our project, look forward to make it real. Base in thinking the levels of impact of the ideas and what they would change and helped by the actual and past actors of the area of intervention.

## Imagine your project in 5 years more

Making the exercise of positioning my project in only 5 years more. Where it will be? What it will change and impact? Who where be your competitors and partners? What your costumers will say about it?

To answer these questions we create a news:

![](assets/t3_3_1.jpg)

*Here are the details of how I imagine my project in 5 year more.*

##Sustainability Model

I Imagined the project in 5 years more, when I have the input and outputs of it totally controlled with the capacity to mycoremediate many materials. With scientific data that proves it. I have a production system to train and grow mycelium in a functional, carefully designed way, to decompose differently contaminates more easily than others can do it.

The business model of the project will be based in a company whose objective is  regenerative design. That looks for the recreation or rebirth of something’s that was dead in different ways with different objectives. Where nature is the one who gives the guidelines.

Here there are previous innovations, research or statements that are the basis of the project:

*  Modern Recycling process, at the beginning of the year 2.000 started a movement that looks at trash lake value material making new polymers with waste, like recycle cardboard, a board made of milk boxes, toys made of plastic bottles, etc.

*  Biomimicry, observes how nature engineering works, designs are made or constructed that imitate these nature processes, applying  them inhuman innovations.

*  Biopolymers, the creation of material that is 100% organic that come from natural materials like algae or starch that can be  thrown in the soil after it is used  without leaving any contamination.

*  Paul Stametts, a mycologist who believes that fungi will save the world creating the 6 ways mushrooms can save the earth.

*  Peter McCoy, creator of the radical mycology network, who develop a training process of fungi to be able to discompose waste.  

*  Biodesign, a concept established by William Myers to refer to the incorporation of living organisms as essential components in the design, replacing industrial or mechanical systems with a biological process. Creating a new hybrid object and architecture that joins biology with humans.

The economic suitability starting point will be made a base in applying to different funding processes to develop different areas of the project. The idea is that the company will be divided into 4 areas:

**Functional Design:** This is the more industrial design one where it is the development of the product (myco-capsule) in the most efficient way that will help everyone to mycoremediate easily in their houses.

**Research:** This is the lab area, where the company investigates and tests how the different scientific and biological aspects operate.. Looking to obtain data of the outcomes and develop new regenerative scopes.

**Networking:** This is the more communicative area, that travels all around the world making talks and workshops teaching this idea.  Promotion of the company creating a network of people that prove the idea and learn from different approaches.

**Service:** This is a similar area to the Functional Design with the difference being here the company develops customized      products to remediate and regenerate material form special contaminates or environments. An example could be an industry that produces contaminants so they order us to develop a specially trained myco-capsule to remediate their toxin.

These 4 areas have different economical sustainability factors because each one  looks at  different ways the regeneration process operates, but every piece of it is necessary to make all of them work,

The functional design idea will be applied via a Kickstarter campaign to win funding to create the product,  it will also to prove  there are people willing  to pay for this object.

The research will be based on science and technology funding around the world, applying to different university or labs that are open to help new innovative ideas in the area.  

The networking will be based on going around universities, school, labs and company’s teaching this new concept. Also making workshop where they can see it works and how real is it. Charging different fees from all of it.

The service will go to industries e.g. cigarette brands, and provide a service to remediate their specific contaminants that they are producing. Providing to then a full development of customize myco-capsules where they can generate material from their waste producing 0% material contamination.

##Reflections

The curse was useful to place my project in the real world, testing it with new people who has a mind inn the business and the sustainability of the ideas, no like normally we have in the master where we can dream freely in utopic worlds. Giving us tools to guide our project to me made in a future not-too-distant creating real impact.  Practicing how to communicate and sell it. Looking forward to find the correct business model.
