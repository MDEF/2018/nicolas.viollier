#Wildcard

I chose for the wildcard week to learn how to use the robotic arm and print clay with it.

![](assets/18_1.jpg)
*This is one of the robotic arms that we have in our Lab, there are two brands ABB and KUKA.*

The robotic arms start like a tool for the car industry. Joseph Engelberg introduces the first robotic arm in 1962, creating the also first company of it called Animation,  installed at the General Motors plant in New Jersey for automated the die casting process. Source: www.ncbi.nlm.nih.gov.

![](assets/18_3.jpg)

*From left to right, Rancho Arm, 1963; Minsky’s Tentacle Arm, 1968; the Stanford Arm, 1969; Silver Arm, 1974*

![](assets/18_2.jpg)
*Actual picture of an industry using robotics arms in their production line where you don’t see any human hands.*

But also a few years ago the brands ABB and KUKA had developed a new type of Robotics Arms more for the area of education, having more easy platforms, less technical and the possibility to connect with 3D models software’s like Grasshoppers. Opening the used of these machines in the area of Architecture and Design.

![](assets/18_4.jpg)
*ABB robotic arm plug inn for Grasshopper software create the perfect file of the movement of the machine.*

![](assets/18_5.jpg)
*Here you can see a KUKA attached with a 3D print filament and an extruder so you can 3D print with it*

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/lX6JcybgDFo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

*Here is a good example of how the robotic arm has open toward the education line a new user creating visual animation art with it.*


##Designing the File

We design the G-Code for the movements of the robots in Grasshopper.

![](assets/18_6.jpg)
*In Food by Rhino, I download the plugin of ABB for Grasshopper, named Taco, where you insert your parametric design and he creates the G-code movements for the robotic arm.*

![](assets/18_7.jpg)
*This is how it looks using the Taco ABB Plugin in Grasshopper.*

![](assets/18_8.jpg)
*The red lines are my design where the robot will move passing his endpoint through the lines.*

![](assets/18_9.jpg)
*These green circles are the start (left) and end (right) points.*

![](assets/18_10.jpg)
*Also, you can check digitally with this timeline (blue) the movement of the robot.*

Finally when you have your file ready cheeked and everything perfect you save the G-code

##Setting the Robotic Arm

![](assets/18_16.jpg)
*We install in the robotic arm a clay extruder that then you put in it a clay mixture and by air precision, it will extrude it.*

Then you need to calibrate the machine settings the XYZ.

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/2FGCIHw-T_0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

*Here we are calibrating it.*


![](assets/18_11.gif)

*Then before starting printing, we also make a movement test without printing clay to check if every movement was OK.*

##Preparing the material

Then we need to make the mixture of water and clay to inserted in the extruder.

![](assets/18_12.jpg)
*So we mix the clay with water until you fill the mixture viscous and start inserting it the extruder very tight training to avoid any bubble inside.*

![](assets/18_13.jpg)
*This part was very messy and analog.*

##Start Printing

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/0vmgNFazOEI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

 *Here is the first clay printing, where you can realize that wasn’t too good because the mixture was to dry it started to break and was difficult to extrude it needing to much air.*

![](assets/18_14.jpg)
*This was the outcome of our first 3D printing, very dry that create a broken shape that we need to stop after a view minutes.*

##Second Mixture

Because the past mixture was to dry we need to create a second one know with more water so the mixture flows better in the extruder and do not gets broken.

![](assets/18_17.jpg)
*This is how it locks the second mixture wetter and viscus.*

##Final Printing

Know having a better mixture we start printing in a more proper way.*

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/uMGIfi_j9Zs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

*Where you can see that the printing has better quality joining perfect every line, but after a few minutes the shape explode because there was a bobble inside the extrude disabling the shape.*

![](assets/18_18.jpg)
*This is how it looks sadly the exploded shape.*

## File
[Clay Printing File](W18_Wildcard/ClayPrinting.gh)
