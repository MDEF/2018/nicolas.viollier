#Electronic Production

The assignment was to make an in-circuit programmer by milling the PCB and then program it.

## Electricity Basic
The first step was to learn the basic of electronic and electricity. Where we had a class of it teaching some things that I already know from pass class of the master but learning other essential things that would help me in the next assignment.

**AC**
Alternating Current (AC) is an electric current which periodically reverses direction, is the form in which electric power is delivered to houses, office and every place.  Is faster, need less voltage and do not have polarity. Because is not direct don’t need big cables.

![](assets/5_1.png)

*AC is a not direct current.*

**DC**
Direct Current unidirectional flow. It is used for electronic that need a constant current like batteries that then provide DC current. Had polarity, is slower but deliver more voltage.

![](assets/5_1.jpg)

*DC flows only in one direction.*

**Ohm Ω**
Is the electrical resistance unit. Measuring the resistance of the flow of electric current.

![](assets/5_2.png)
*Electrical resistance*

**Capacitor**
Is an electric condenser (keeps energy like a little battery) able to store energy supporting an electric field.

![](assets/5_3.jpg)
*There are different types of capacitors with different farad (F) units that is the electric capacity store.*

**Diode**
Is a terminal electronic component that conducts energy in one direction. Useful to the delivery energy in only one direction, like a gate, when you are using a battery or a capacitor.

![](assets/5_4.jpg)
*The diode cut the energy form the with mark to the left.*

##Milling the PCB

Milling the PCB is the next step.

1. The first step prepares the PCB copper board.

![](assets/5_6.JPG)

*You put double sided tape and stick it in the CNC milling machine board.*


2. Second prepare the files.

![](assets/5_7.png)
*PCB Trace*

![](assets/5_8.png)
*PCB Interior*

![](assets/5_19.JPG)

*In the computer of the milling machine you insert the file of the trace and interior cut and then you calculate the CNC movements. Setting up the end mills for the trace a 1/64 and for the interior a 1/32. After that, I create the file and save it.*

3. Third set up the XYZ and the end mill.


*Firs we mill the trace, so we put a 1/64 end mill and carefully calibrate the Z and then looking for have enough space set the XY.*

4. Fourth start milling

![](assets/5_9.JPG)
![](assets/5_10.JPG)
*Finally you set the file that you save before and it will begin milling immediately. I change the speed of 70% to make a more smooth mill.*

###Problem and Learning through the making.

The milling looks like something easy but has many issues or processes that you need to have control of it and be sure that they are correct to have a fast and correct result, that I haven’t know.  

**Cheek is the scale is the correct**

![](assets/5_11.JPG)
*When the milling finished I realize that it mill the PCB very tiny is not the correct scale. So I realize that when I create the file something I change and it scales the dimension of the PCB. So then I create again the file and make sure that the final dimension was the correct.*

**First clean milling machine base before sticking the PCB copper board.**

![](assets/5_12.JPG)
![](assets/5_13.JPG)
![](assets/5_14.JPG)
*Finally I could mill the trace having an excellent result but when I began to cut the interior the board began to move and stick out of the machine base losing my XY set up. Learning that I need to clean first the base and then stick the PCB board in a place where is more flat. So I need to cut again the trace after cleaning and sticking it properly.*

### Final result
![](assets/5_15.JPG)
*Finally I mill my PCB correctly. In a next PCB board, with a new XYZ step and position in milling board*

![](assets/5_16.JPG)

 *I use a Roland SRM-20 machine.*

![](assets/5_17.JPG)
*Final result, after cutting the interior with a box cutter you take out the board and separate it.*

![](assets/5_18.JPG)

The final process before welding is to take out the burr with alcohol and a metal scrubber to do not have connections problems then.

##Soldering My PCB

First I create in a piece of paper the list of electronic elements that I would need to solder to the PCB and then in the fab lab electronic library look for each of them and with tape stick them to the paper. So when I will be soldering I would not have problems looking for them being the focus in the soldering process.

![](assets/5_21.JPG)
*My list of elements.*

Second I started soldering. Where it was at first very difficult because the precious needed and how to work with the different tools need some practice.

![](assets/5_22.JPG)
*Was very important to have a clamp to sustain tightly the PCB.*

![](assets/5_20.JPG)
*When I finished soldering my PCB, some pieces began to fold down because wasn’t correctly solder done learning that the solder needs to shine.*

##Programing My PCB

First I test with a multimeter to see if the connections were correct.

![](assets/5_23.JPG)
*Realizing that my PCB has many problems, bad soldering, crossing connections and the microcontroller was connected in a wrong direction.*

![](assets/5_24.JPG)
*Fixing the many problems that my PCB have creating sadly a more dirt electronic piece because all the problems that I have and my wrong soldering.*

Finally programing my PCB see how these connections began to be something real electronics.

![](assets/5_25.JPG)
*Happily the programmer tour his light green communicating that was correct and ready to program.*

After installing and coding the different step to program my PCB y finally program it successfully.

![](assets/5_26.jpg)

*Satisfactory moment when the programming process was finished.*

![](assets/5_27.jpg)
*Sadly when I test if my computer recognizes my PCB it haven’t read it finding a new problem. I tried with different computers having a bad result with all of them.
Rechecking the soldering and changing the diodes don’t solve the problem also.
I left the final final step unfinished.*  
