#Networking and Communications

We went through the process of make a communicator device that is able to send message and data to another device.

![](assets/13_00.jpg)

We made this project in a group of 4 and the members where;**[Adriana Tamargo](https://mdef.gitlab.io/adriana.tamargo/FabAcademy.html).**,**[Alexandre Acesensi](https://mdef.gitlab.io/alexandre.acsensi/fabacademy/w14/w14.html).**,**[Laura Alvarez](https://mdef.gitlab.io/laura.alvarez/fabacademy/week15_apr24/).** and me.

Because we were a big group we take advantage of it a made a bigger and complete assignment.

## Programming

To make a communication device system we work with an Arduino board and a communicator device NRF24.

**What is an NRF24?**

![](assets/13_0.jpg)

Is a single chip, that has the electronics and precise functional blocks to establish RF (Radio Frequency). Being able to communicate between two or more points at different speeds, up to 2 Mb/sec, sending and receiving messages. Without external control interventions insolating us form physical transmission.

### Connections

First, we connect the NRF26 communicator with the Arduino.

![](assets/13_2.jpg)
![](assets/13_3.jpg)

*Following the instruction of this map we were able to connect it correctly.*

![](assets/13_1.jpg)

*This is how it look connected.*

Then in the computer in the Arduino program we download the RF24 library and import it into our Arduino IDE.

After that we send the next code that was the sender code:

const int pinCE = 9;
const int pinCSN = 10;
RF24 radio(pinCE, pinCSN);

const uint64_t pipe = 0xE8E8F0F0E1LL;   

char data[16]="Hola mundo" ;   

void setup(void)
{    
     radio.begin();    
     radio.openWritingPipe(pipe);
}   
void loop(void)
{    
   radio.write(data, sizeof data);    
   delay(1000);
}

For then repeat the same process in another computer with another Arduino board and NRF24 communicator but with the difference that in this case we will use the receiver code:

const int pinCE = 9;
const int pinCSN = 10;
RF24 radio(pinCE, pinCSN);

const uint64_t pipe = 0xE8E8F0F0E1LL;   

char data[16];   

void setup(void)
{    
  Serial.begin(9600);    
  radio.begin();    
  radio.openReadingPipe(1,pipe);    
  radio.startListening();
}   
void loop(void)
{    
   if (radio.available())    
   {       
       radio.read(data, sizeof data);        
       Serial.println(data);    
    }
}


Having emotionally our first communication with this outcome:

![](assets/13_4.gif)

## Sending our Messages

But then when we try to put a longer sentence that you modified in the code:

char data[16]="Hola mundo" ;   

We change for:

char data[16]="Hola K ase Laura que tal con los Jordis?" ;  

But in the sender code wasn’t sending the complete message only showing "Hola K ase Laura"

So we realize that the "16" number in the char data was the number of characters so we changed for "300" sending it correctly.  

![](assets/13_6.jpg)
*Complete message send.*

But then in the receiver, we change also the data number but we have another problem.

![](assets/13_7.jpg)

*Not showing the whole sentences and showing some weird numbers and codes.*

So we learn that the NRF24 communicator board has only 32 bytes capacity that means that it only has 32 characters to send.

![](assets/13_8.jpg)
*Learning about the bytes and beat here, finding that one byte can store one character.*

![](assets/13_9.jpg)
![](assets/13_10.jpg)

So we then test this, coding numbers to 0 to 9 repeating this fraction again so then we can see easily and check if there are really 32 characters the one that we can message. Figuring out that was true that were 32 bytes = 32 characters.

Realizing that we need to spread the sentence into char.

char data[16]="Hola k ase Laura" ;   
char data[16]="q tal con los Jordis?" ;   


![](assets/13_11.jpg)

Having this like an outcome.

##Creating our ASCII code

ASCII code is like the DNI that use the NRF24 communicator, where you need to have in the sender and the receiver the same code to send the data or message.  

The ASCII code is in the pipe coding part of the Arduino where the default one was:

const uint64_t pipe = 0xE8E8F0F0E1LL;   


**ASCII** is the stand for American Standard Code for the Information Interchange. Computers can only understand numbers, so ASCII code is the numerical representation of characters like "b" or "@". Was design for teletypes systems. Source: www.asciitable.com

Here you can see the ASCII table:

![](assets/13_12.jpg)

![](assets/13_13.jpg)

*Creating our own ASCII code that was 47474747. So now we can have our private messages.*


##Sending sensor data

Our final step was to send information of a sensor from the Sender to the Receiver.

![](assets/13_14.jpg)

*So we connect to the Arduino of the sender a light sensor. Coding then in the char to send the data of the sensor.*

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/vxfCNBKHetw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
*Having finally in the Arduino Sender the data of the sensor.*

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/nWk7f3wqhLA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

*So, then we code the in the Receiver, that was incredibly receiving the data of the Sender light sensor, that if the data was over 200 a red led turn on.*

But then when we put a higher number like 600 to make a bigger change we couldn’t change it because we couldn’t code to transform the "char" to "int". So we weren’t able to see a real led light change.


Arduino Files:
[Sender:Sensor_with Two messages and costume ASCII](W14Networking and Communications/Sender_Sensor/Sentences/ASII.ino)

[Receiver:Network and costume ASCII](W14Networking and Communications/Receiver_Network.ino)

[Receiver LED Sensor Interaction](W14Networking and Communications/Reciever_Led.ino)
