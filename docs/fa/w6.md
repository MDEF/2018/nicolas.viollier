#3D Scanning and Printing

The focus of this week was to learn about 3D printing and 3D scanning

During my university degree, I work allot in 3D printing, making my final project about it and how they can work together with the manufacturing industry. **[Here you can cheek my final project](https://www.behance.net/gallery/41506103/Custom-Industrial-Gauge).**

Also, I have done many freelance works of deigning especial 3D print pieces that need to fit in something. Because I haven’t Kwon about 3D scanning a need to waste a long time measuring and modeling to create a product that fits.

So my focus this week will be in learning about 3D scanning, how to use it and post-production.

![](assets/6_1.jpg)


##3D Scanning

The scanner that we will use is Xbox 360 Kinect, it is officially made for game scanner functionalities in the Microsoft console Xbox. But is a great tool that can be hacked to function as a 3D scanner. Be easily supported in any graphics card of normal computers.  

![](assets/6_2.jpg)
*Xbox 360 Kinect*

Also, we need a program that processes all the information received from the Kinect. This will be Skanect, that is a very simple program easy to use for beginner’s that do not have professional equipment’s to create 3D meshes and enter to the world of 3D scanning without needing special scenarios.

![](assets/6_3.JPG)

*Learning to use Skanect and Kinect. Having some issues because I was moving.*

##My Scan project

To learn about 3D scanning and postproduction I thought in something that I need in my daily life and would make me reflect on the usability of the 3D printing system.

I choose to create a special revolving stick for the bottle that I use in the home to make a powder juice.  That had the problem that its bottom is to curve so the power is never well mixed keeping in the bottom some powder.

![](assets/6_4.JPG)

*This is the bottle.*

![](assets/6_5.JPG)

*This is the curve that don’t let you mix properly.*

![](assets/6_6.gif)

*Here you can see how I actually revolve the juice without reaching correctly the bottom.*


So the idea is to scan the bottom of the bottle and copy its curve. To make a special tool that fits in it and removes all the powder.

![](assets/6_7.JPG)

*Because bottle is transparent I need to cover the bottom with something because the Kinect do not read transparent thinks and either shiny things. I decided to cover it with a balloon and but tape in the central bottom so it covers completely the bottle curve.*

![](assets/6_8.JPG)

*My first scanning test wasn’t good because I was moving the scanner instead of the object, generating an irregular shape.*

![](assets/6_9.JPG)

*Final scanning position, where I located the Kinect over some chairs looking down the object that was in the top of a rotation system made of an upside-down rotary chair with a circular wood over its fits.*

![](assets/6_10.jpg)

*So I push to play in the scanning program and then began slowly to rotate the bottle.*

**Final Result of the Scanning**

![](assets/6_11.png)
*This is the best result of the scanning.*

![](assets/6_12.png)
*After postproduction using the program mesh mixer I create a more smooth and useful 3D model.*

![](assets/6_13.jpg)

*Finally in Rhino I cut and clean it to create the product that I want to 3D print.*

## 3D printing

At first, I want to 3D print the whole stick mixer but was useless to 3D print it, that need to be 30cm long being a waste of time and material. So I design a mixer gadget with a hole in it where it will fit the stick, that was actually a kitchen clamp, where I currently use to mix.

![](assets/6_14.JPG)

*For that I measure the bottom of the clamp so it would fit correctly and tight*

![](assets/6_21.jpg)
*Having finally the 3D model of my gadget special mixer. Without any offset because I wanted to it fit very tight enough that I would fold down when I mix*

![](assets/6_22.jpg)

*Also I create a 3D model that in the hole it would have little bumps to fit in the circular hole that have the clamp in case that it does not fit tight enough. So I would 3D print the two models and then test what is the best.*

![](assets/6_23.JPG)

*Finally I started 3D printing my mixer. The material selected was TPU that is flexible and elastic so it could help to be more tight to the clamp without needing a perfect shape and adapt to the bottle shape.*

![](assets/6_24.JPG)

*This was the parameters used, selection 30% of infill so the product is hard to move the juice without needing support because I locate the model's upside down.*

###3D Print Results

**The result was really good**, the dimension of the hole was narrower but it helps to fit the stick tightly, the material was very flexible but hard ad the same time and the scan curve was in a really good definition.

![](assets/6_25.JPG)

*This was the result of the 3D Printing.*

![](assets/6_26.JPG)

*Fitting very tight, using the model without the bumps, like I wanted. The hardness and elasticity of TPU material help to hold the gadget properly.*

![](assets/6_27.jpg)
![](assets/6_28.gif)

*Removing the juice powered and reaching the curve bottom of the bottle correctly.*

![](assets/6_29.gif)

*Using know in my daily life a 3D printed gadget made by my self testing the usability of these new technologies.*

## Scanning Me

To learn more about 3D scanning I decided also to 3D scan me.

![](assets/6_15 .JPG)

*Using the same scanning system that I use with the bottle but in this case, I was sitting in the rotary chair, we put more light and someone needs to move me slowly and I need it to do not to move.*

Making funny render of my 3D Scan. Showing that after regulating the light, the distance and the movements systems you can have good results.*

![](assets/6_18.jpg)

*I made of gold.*

![](assets/6_17.jpg)

*I made of copper.*

![](assets/6_16.jpg)

*I made of water.*

![](assets/6_19.jpg)

*I made of shiny plastic.*

![](assets/6_20.jpg)

*I made of carbon fiber.*


## Files

[Bottle Stirrer 3D file](W5_3D Scanning and Printing Files/Stirrer.3dm)

[My Body 3D model](W5_3D Scanning and Printing Files/MeScaneed.stl)
