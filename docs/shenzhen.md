#Shenzhen Experience Reflection

The Silicon Valley of Hardware. 40 year ago was only a little town of fisherman’s and now is a giant city with more than 12 millions of citizens, vibrant, active, and full of young people with energy to make and produce technology. It is the place where almost all the technology products of western brands that we used daily are made, like iPhone's, computers, etc. They are in a process now also to develop their own brands and also manufacture it creating brands like Huawei and Oppo.

Shenzhen is part of Deng Xiaoping, communist leader the father of the economic reform, plan at 1979 to create 4 special economic zones to let foreigner’s inverts and create a private business. With also the advantage of being near Honk Kong, this old little fisherman village was converting the fastest growing city in the history of the world, with more than 50 skyscrapers that have more 200 meters of height and others 50 more skyscrapers in construction.

![](assets/SH2.JPG)
*Shenzhen is the paradise for makers.*

##The power of Chines manufacture system

Chinese over the traditional generation we observed an attitude and cultural values of **doing your best in what you are working in**. Seeing it in the traditional handcraft and now in the actual industrial manufacture process.

They have cultural work ethics that make a strong potential relationship with the making and the outcomes.

During the trip we observe them working and is incredible how tuff workers are they very focused in what they are doing without any lazy moments.
![](assets/SH1.jpg)
*Inyection factory motivational motto.*

###Manufacture systems of doing everything

Creating thanks to this work ethic a manufacturing system where Chinese cities are surrounded by giant industries that can make everything. Creating a country that has the capacity to produces in enormous scales quickly with excellent quality whatever you can imagine.

![](assets/SH3.jpg)
*Plastic inyection factory building and manufacture space.*

##Maker movements

Generating Shenzhen a city surrounded by hardware industries where you could manufacture whatever technologies idea you can imagine on a big scale.

**Having the biggest technologies market of hardware in the world, Huaqianbei**, where you can find all the technical electric pieces of technology products. Having thousand of little market shops direct from the factory, where you can make your ideas in the cheapest way and with most new technologies

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/leFuF-zoVzA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
*Creating an iPhone by buying all the pieces in Huaqianbei, demonstrate that the market has all you need.*

![](assets/SH5.JPG)
*David Li director of SZOIL Open Innovation Lab of Shenzhen "In Europe or USA if you want to create product first you make a team, then you need to create a good work environment, then you need to make them work together, then they have creativity scions, then they quote in China and finally after 2 years after they start they finish the product. Here in China is more efficient they talk together about the project they arrange an idea, then they go to the factory and after 3 months they can have it in the market."*

##The necessity of Design

We realize that Chine has a big problem where they can produce whatever they think but their knowledge of design and research is just starting. Seeing in many of their creation the power of having all the worlds’ technologies but without a design concept that would help in its functionality and its aesthetics. That European people are expert.  

Where you can see it in many of their product innovation like their cars, the motorcycle’s, robots, etc. and also in digital graphic design like the metro wayfinding, mobiles, apps, etc.  

X Factory, a digital fabrication lab for markers, has realized the lake of innovation in Chinese manufacture industry. Where the factory has been doing the same product’s all the time so X Factory is helping them to innovate and began to create new products with design concepts.

![](assets/SH6.JPG)
*Vanke Construction Company with X Factory, are starting to mix digital fabrication in construction.*


##Not scared about copy

China communism governs, does not care about the western copy laws, promoting the open dialogue and the share of ideas. Where is permitted to copy foreign brands, see in Huaqianbei market all type of excellent copies of iPhone's, Samsung and American cloths brands? **But is not permitted to copy local companies with a big penalty if you do it.**

![](assets/SH7.JPG)
*During a visit in the Chinese biggest company of medical equipment Mindray. They comment us that they do not have problems with copies because Chine governs do not let coping local companies only foreign.*


##A real Tech City

When you imagine future city’s, Shenzhen is the perfect example of what they would be in 50 years more, but it is happening now.

**With the advantage of having all the technology manufacture in there hand** and with chines production and maker system that is fast and efficient, they can create whatever they imagine in a very cheap and fast way with the newest innovation of the world.

Where the citizens do not use the money to pay with, they use their phone to pay to scan a QR code, in supermarkets, metro, street food, taxi, etc. Using to pay app like We chat and Alipay.

![](assets/SH9.jpg)
*Street food with a QR code pay system.*

All the motorcycles, taxi and buses are electric with excellent efficiency and very fast all made in China. Improving air pollution and also noise pollution.

![](assets/SH8.JPG)
*The most famus Electric car company is BYD*

The food delivery system is incredible all the people used it to eat daily. Where you can order starting with a cup of coffee to whatever food you what. Without human interaction and tips, the deliverymen will leave it in time on a table or where the app indicates for them to go.

![](assets/SH10.jpg)
*Shenzhen delivery company*

An illuminated city where Chinese architect’s play in an incredible way covering the big buildings with facade lights and LED screens, making every of it like a mapping LED show that surprise you every day. Transforming the city at the night making it more energetic and active demonstrating the real Chinese technology.

![](assets/SH11.gif)

*Incredible show light facades*

Europe cities are showed like ecologic and technology places but now knowing Shenzhen we realize that the change that this western city have are very little and they are more marketing than reality. Having some recycler points and electric buses but this is not real changes and does not represent an innovatively sustainable city with real system changes like the one we saw in China.  

##Custom PCB delivery

Seeed Studio (2008), a company that design and manufacture PCB in large scales and small scales. Opening our minds in the manufacture of our ideas, helping us in a future step where we want to convert our Arduino in real products.

Seeing an opportunity in future of Seeed Studio making more near and affordable the manufacture of technology products. Being the connector with the manufacturing industry with their contact’s and technical skills. Helping designers to create real innovation, not on a prototype scale.

![](assets/SH4.jpg)
*Visit PCB industry manufacture*

##Could China eat western countries?

China used to be a very poor country with a high unemployment rate but through some economic governs changes and the hard work cultural ethics. They have been massively growing,  but steal in the level of a developing country.

China, being the center of the industrial manufacture of the world. They are now in a process where they have realized that their knowledge in a production system is so great that they can export it and beginning to create factories outside of China.

Also beginning to improve their design systems and create a brand like the technology brand Huawei and Xiaomi launching product’s that are similar or more innovative that western brands with more acceptable prices and have the power to grow more than them. Making me think that in 20 year’s more we will all began to use Chinese mobile brands phones.

![](assets/SH12.jpg)
*Xiaomi store would be the most popular tech brand in a few years, with his innovative designs and seeing tech no  only in phones and laptops in other usual objects.*


##Business Sustainability

During the visit, we realize that Chine is strange sustainable country. Because in one side you see many electric vehicles’s, the city and also the houses, building, and offices is full of green parks, they like to eat fresh fish, meat and vegetables But on the other side they do not care about pollution, they love plastic giving using as many as possible plastic bags, they aren’t used to recycle.

Realizing that they do not care about the global warming and make the world greener for better future like western thoughts, they see the nature different like a business substantiality of what help them at that moment, help there work energy and their family future.
![](assets/SH13.jpg)
*Chinise people see nature in different way*

![](assets/SH17.gif)

*Shenzhen citysens choosing meticulously the better fruit and vegetables*

Also, all their makers’ ideas are not focused on ecologic energy and biology innovation. They do not use nature, like biomimicry, to help and go in ideas.

But in the other said making real changes for a better world like what was explained before electric cars, training to eat the freshest food as possible, full of green parks.

Showing us that sustainability and care of the world are not just using fancy cardboard packaging, filling the streets with recycler’s spaces and bamboo bikes that are not real changes for a better world.


##Craft the point of union with nature

Chines traditional old craft shows that their culture of hard workers is not only now with their manufacture revolution, is something traditional passed bay generation by generation but focused on different objectives.

Observing the old chine craftsmanship they are incredible products made by hand with natural recourse solving real necessities.

![](assets/SH15.jpg)
*Old fish net system made by bamboo now used for architecture spaces*

**Could be the craftsmanship the point of union of biology and manufacture system? To solve this sustainable issue and make China a place of sustainable developments.**

![](assets/SH16.jpg)
*Craf: The Reset. Exhibition of designers using old Chinese craft tecnic with new technologies, in the Design Society Museum*

##Conclusion.
*Chinese with there communism govern control are making a team work system to make China the most developed country in the world.*
![](assets/SH14.jpg)
Shenzhen is a vibrant city that opened the gates of manufacturing for me, and now I have some contacts and a greater understanding of it making processes. Teaching me the power of technology the usability of it, where it can go in and the scale. Helping me imagine the future, the advantages, and problems, to look forward and create new designs for upcoming times.
