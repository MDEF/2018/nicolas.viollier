#Nicolás Viollier
##Industrial Designer          
.................................................................................................................
![](assets/ABT.jpg)
*"I am  Industrial Designer of Chile, based my works in digital fabrication and haw this new technologies can help the industry production make a circular world production."*

###Work experience


####Downlight / Light Company						               December 2015 - August 2018
Marketing manager, in charge of all the advertising and image of the company. Opening the marketing area, in charge of the digital, industrial and graphics design. Creating a work team.
Marketing Manager / Renca / Santiago, Chile.

####UC / Design School							       August 2015 - December 2015
Project Manager in charge of an interdisciplinary team of interns, presently designing a Children Olympics Event for Llay-Llay County / San Felipe, Chile.
Lead Designer at street fair county project / La Florida County / Santiago, Chile.

####Arbol Color Diseño                                                                                                              December 2014 - January 2015
Professional Internship, Assistant of the interior and interaction design area / Santiago, Chile.

####Pastoral UC 								       August 2014 - December 2015
Social Internship, Manager of setting, communication, graphic and advertising design / REC Project / Santiago, Chile.

####UC / Design School 								   March 2014 - July 2015
Teacher Assistant of:
•	Product and graphic design workshop, fourth semester students / 80 students / fourth teachers / fourth assistants.
•	Introduction design theoretical class, first semester students / 40 students / two teachers / two assistants.
•	Industrial workshop of paperboard products, sixth semester students / 12 students / one teacher / one assistant.
•	Industrial workshop of emergency products, sixth semester students / 10 students / one teacher / one assistant.
•	Industrial workshop of paperboard products, sixth semester students / 12 students / one teacher / one assistant.

####Almacen UC						                                                  March 2014 - July 2014                                                                                                                           
Designer of interactive games with native wood for University shop / UC University Shops /
Santiago, Chile.

####Novoplast                                                                                                                                         March 2011 - July 2011
Worker Internship, Assistant of Quality Control Department, PVC pipe company / Santiago, Chile.

##Education
Pontificia Universidad Católica de Chile
BFA. Design    2011 - 2016  Graduated with honors

**[My portfolio link](https://www.behance.net/nviollier).**
